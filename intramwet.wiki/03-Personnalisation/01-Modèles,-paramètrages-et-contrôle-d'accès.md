Pour fonctionner, la plateforme a besoin d'un certain nombre de modèles Django qui permettent de définir différentes catégories et les différentes règles d'accès.

Toutes les initialisations se font dans le module `init` de l'application `intramwet.core`.

Pour déclencher l'initialisation une fois les objets d'initialisation définis, utilisez la commande suivante :  

```bash
docker-compose run web python manage.py init
```

## 01 Initialisation du module `utilisateur`

### Le modèle `django.contrib.auth.models.Group`

Il s'agit du modèle de groupes par défaut de Django. Il permet de définir des catégories d'utilisateurs, et d'automatiser le contrôle d'accès (pour plus d'informations sur les groupes Django, voir [ici](https://docs.djangoproject.com/fr/3.1/topics/auth/default/#groups).

Pour ajouter une permission à un groupe, vous pouvez utiliser quelque chose comme :

```python
p_id = Permission.objects.get(codename='permission_codename').id
g = Group.objects.get(name="Group name")
g..permissions.add(p_id)
```

### Le modèle `StatutCA`

Les `MembreCA` ont des `StatutCA` (President, Treso, etc.).

Ce modèle permet d'automatiser l'accès à l'interface de passation, au module treso et à l'interface admin.

Un statut a un poste - Président, Trésorier, Secrétaire, Développeur, Gestionnaire cinet (Cinéma-études) et Autre, dont le but est de prédéfinir des types obligatoires pour la gestion de l'association de part ses statuts.

### Le modèle `DescriptionGroupe`

Ce modèle permet de définir des descriptions pour les `Group` Django par défaut pour l'interface d'inscription.

### Le modèle `CorisationGroupe`

Ce modèle permet de définir les cotisations pour les `Group` Django. Si un groupe n'a pas de cotisation définie, il est considéré comme un groupe qui n'en nécessite pas pour l'enregistrement.

## 02 Initialisation du module `materiel`

### Le modèle `ProprietaireMateriel`

Les `Materiel` appartiennent à des `ProprietaireMateriel` qui permettent de faire du contrôle d'accès et de prix.

Chaque `ProprietaireMateriel` a deux permissions associées créées automatiquement :

- `emprunt_gratuit_proprietaire_{id}` : autorise l'utilisateur à emprunter de manière gratuite tous les matériels appartenant à ce propriétaire ;
- `emprunt_payant_proprietaire_{id}` : autorise l'utilisateur à louer tous les matériels appartenant à ce propriétaire.

Si aucune de ces deux permissions n'est accordée à l'utilisateur, les objets associés au propriétaire n'apparaissent pas dans l'interface d'emprunt pour cet utilisateur.

### Le modèle `TypeMateriel`

Les `Materiel` sont organisés en `TypeMateriel`, qui permettent de les classer dans l'interface d'emprunt.

## 03 Initialisation du module `projet`

### Le modèle `TypeProjet`

Chaque `Projet` a un `TypeProjet` associé.

Chaque `TypeProjet` a une permission associée `voir_type_projet_{id}` autorise l'utilisateur à voir tous les projets d'un type donné.

### Le modèle `TypeParticipantProjet`

Chaque `Utilisateur` peut s'inscrire à un projet en tant que `TypeParticipantProjet`.

### Le modèle `PreconfigurationTache`

Chaque objet `PreconfigurationTache` permet de définir des raccourcis lors de la création de projets, notamment pour le paramétrage des notifications, du nombre de personnes assignables, etc.

## 04 Initialisation du module `treso`

### Le modèle `CategorieGrilleTarifaire`

Les `EntreeGrilleTarifaire` sont classées en `CategorieGrilleTarifaire`.

## 05 Initialisation du module `documentation`

### Le modèle `Categorie`

Tous les `Document` sont classés en `Categorie`

Chaque `Categorie` a une permission associée `acceder_categorie_{id}` créée automatiquement qui autorise l'utilisateur à accéder à tous les documents d'une catégorie.