![Capture_d_écran_2020-09-13_150919](uploads/369b4af1254a84aaeb57a9962ef642ba/Capture_d_écran_2020-09-13_150919.jpg)

Les utilisateurs du site peuvent s'enregistrer dans différents groupes, qui leur permettent d'accéder à des fonctionnalités qui leurs sont spécifiques. Ces groupes sont paramétrables selon les besoins de l'association (on peut en rajouter, en enlever, changer les permissions d'accès, etc.). 

## Gestion des adhésions à l'association

![Capture_d_écran_2020-09-13_150403](uploads/bffe7d2b7627c6a6d10822b0cca8a162/Capture_d_écran_2020-09-13_150403.jpg)

La plateforme permet de gérer les adhésions des membres, avec une génération de bulletin d'adhésion et suivi du moyen de paiement utilisé par l'utilisateur. Selon si le membre a payé sa cotisation ou pas, son accès aux autres modules du site peut être limité (par exemple pour ne pas lui demander la possibilité de participer à un projet ou emprunter du matériel).

![Capture_d_écran_2020-09-13_150835](uploads/c253aa3771c374007e231482bd77d5bc/Capture_d_écran_2020-09-13_150835.jpg)

Les cotisations pour les différents groupes de membres sont paramétrables, ce qui permet notamment de distinguer les prix pour les membres VA et les externes. Il est aussi possible de définir des comptes qui n'ont pas accès au module d'adhésions (pour les externes, les autres associations, etc.).

Le processus d'enregistrement sur le site est ainsi séparé en deux parties :
- une première validation qui active le compte de l'utilisateur et lui permet de se connecter ;
- une étape d'adhésion à l'association ;

## Gestion des membres

![Capture_d_écran_2020-09-13_150215](uploads/0958b956bd7171818a5572646b5cda14/Capture_d_écran_2020-09-13_150215.jpg)

La plateforme permet à ses utilisateurs de voir l'ensemble des membres de l'association (avec fonctions de recherche) pour faciliter la communication ; elle permet aussi d'identifier les représentants de l'association (bureau, CA) sur les différentes années.