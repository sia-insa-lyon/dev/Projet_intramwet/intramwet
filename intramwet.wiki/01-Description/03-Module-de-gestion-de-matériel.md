Le module de gestion de matériel permet d'inventariser et d'effectuer un suivi simple du matériel de l'association.

## Emprunt de matériel par les membres

![Capture_d_écran_2020-09-13_150556](uploads/568f2074a96c0cc0efe18c90414889d5/Capture_d_écran_2020-09-13_150556.jpg)

La plateforme permet à ses utilisateurs d'emprunter du matériel. Selon le groupe de l'utilisateur, le matériel proposé est différent, et la plateforme permet aussi de faire des locations payantes (avec édition de facture pour l'emprunt).

Le matériel est aussi différenciable par son propriétaire (matériel propre de l'association, matériel CVA, etc.) ; ces propriétaires sont paramétrables et permettent aussi de limiter l'accès au matériel à des groupes spécifiques.

![Capture_d_écran_2020-09-13_150515](uploads/91ab16b459e0486377743ec44afd38ec/Capture_d_écran_2020-09-13_150515.jpg)

Les mêmes fonctionnalités sont disponibles pour l'emprunt du local de l'association.

## Gestion des emprunts

![Capture_d_écran_2020-09-13_150627](uploads/81578919243d8dd2b05c7639cd749d22/Capture_d_écran_2020-09-13_150627.jpg)

L'interface de gestion des emprunts par les administrateurs permet d'accepter/refuser tout ou une partie d'un emprunt, et de rajouter des éléments si besoin. Elle permet aussi d'avoir un historique des emprunts et d'effectuer un suivi du matériel (en pointant quand un élément a été retourné).