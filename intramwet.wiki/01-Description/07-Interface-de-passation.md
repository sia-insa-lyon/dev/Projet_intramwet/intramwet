![Capture_d_écran_2020-09-13_145931](uploads/28e51efe2a5e8b7ba0543eed31b3a093/Capture_d_écran_2020-09-13_145931.jpg)

L'interface de passation permet de changer les administrateurs de la plateforme en fin d'année, de télécharger une archive de toutes les données générées pendant l'année et de nettoyer la base de données avant la nouvelle année.

Les différents rôles disponibles au sein du CA/Bureau permettent de limiter les accès aux différents administrateurs pour séparer les responsabilités.