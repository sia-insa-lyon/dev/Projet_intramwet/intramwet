![Capture_d_écran_2020-09-13_150732](uploads/6d483ef8e2d0f10c56de18293101075c/Capture_d_écran_2020-09-13_150732.jpg)

Toutes les données juridiques de la plateforme sont modifiables par les administrateurs du site. Ceci permet de changer facilement l'adresse juridique, la description, le numéro SIRET/SIREN, etc., sans avoir à redéployer la plateforme.

![Capture_d_écran_2020-09-13_150805](uploads/defd9b93bc766d58e2e3d018b58764b3/Capture_d_écran_2020-09-13_150805.jpg)

Tous les éléments graphiques de l'association sont aussi facilement modifiables.