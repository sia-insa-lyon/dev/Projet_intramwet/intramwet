![Capture_d_écran_2020-09-13_145653](uploads/9146dbb33b677645300f3bf5becee9bb/Capture_d_écran_2020-09-13_145653.jpg)

![Capture_d_écran_2020-09-13_145803](uploads/9b1a5f5a7b33845aac3c0f24bca11213/Capture_d_écran_2020-09-13_145803.jpg)

Le module de documentation permet de rendre accessibles des informations importantes pour la vie de l'association, avec des restrictions d'accès selon le groupe de l'utilisateur. Ceci permet notamment d'expliquer comment fonctionne l'association aux différents types de membres, de transmettre des formations, etc. Tous les documents sont téléchargeables.

![Capture_d_écran_2020-09-13_145828](uploads/4ff16f0bdbd3b70fad8d7ca3627eeb97/Capture_d_écran_2020-09-13_145828.jpg)

Les documents sont rédigés au format Markdown, et il est aussi possible de rajouter des images. Chaque document est associé à une catégorie. Ces catégories sont personnalisables, et définissent les droits d'accès de chaque groupe.
