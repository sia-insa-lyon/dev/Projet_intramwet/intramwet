## Génération de factures

![Capture_d_écran_2020-09-13_150013](uploads/63ddb127862c8053e654fad1c7fee3e7/Capture_d_écran_2020-09-13_150013.jpg)

La plateforme permet de générer des factures au nom de l'association, en simplifiant la gestion de la trésorerie, avec un historique de toutes les factures et devis édités sur l'année. 

Les représentants des associations "clientes" sont regroupées dans des organisations pour permettre un suivi des factures d'un mandat à un autre.

Une grille tarifaire permet de définir des pré-paramétrages pour l'édition de factures, mais ces pré-paramétrages restent modifiables pour permettre à l'association d'adapter ses prix.

![Capture_d_écran_2020-09-13_171254](uploads/c0b6242fd154a871a5b21f2f6b694bc7/Capture_d_écran_2020-09-13_171254.jpg)

Les factures sont éditées avec l'ensemble des informations nécessaires à la facturation (adresse de l'association et du client, prix, mention d'exonération conforme à la loi 1901, et SIRE le cas échéant).

## Génération de notes de frais

![Capture_d_écran_2020-09-13_150050](uploads/15ef83b768f70c82b8feebbac8f218b3/Capture_d_écran_2020-09-13_150050.jpg)

En parallèle à la génération de factures, la plateforme permet de générer des notes de frais pour rembourser les dépenses avancées par les membres. 

Ces notes de frais comportent l'ensemble des informations nécessaires pour le suivi des remboursements (moyen de remboursement, motif, coordonnées du membre remboursé). Elles sont générables par les administrateurs de l'association (l'accès à ce module pouvant être restreint à des membres particuliers du bureau comme le trésorier ou le président).