**intramwet** est une plateforme de gestion associative permettant de gérer :
- les membres et l'adhésion des membres ;
- le matériel et l'emprunt de matériel ;
- les projets de l'association ;
- la trésorerie de l'association, dont l'édition de factures, devis et notes de frais ;
- l'organisation administrative (CA/Bureau) et la passation des fonctions ;
- la documentation de l'association ;
- la communication entre les membres et le bureau par mail et via un bot Discord.

Le but de la plateforme est d'être facilement paramétrable et extensible (en permettant aux administrateurs de modifier les informations de l'association et les éléments graphiques sans modification de code)

## Contenu

### Description

- [Interface d'accueil personnalisée](01-Description/01-Module-d'accueil)
- [Module de gestion des membres](01-Description/02-Module-de-gestion-des-membres)
- [Module de gestion de matériel](01-Description/03-Module-de-gestion-de-mat%C3%A9riel)
- [Module de gestion de trésorerie](01-Description/04-Module-de-gestion-de-tr%C3%A9sorerie)
- [Module de gestion de projets](01-Description/05-Module-de-gestion-de-projets)
- [Module de documentation](01-Description/06-Module-de-documentation)
- [Module de passation](01-Description/07-Interface-de-passation)
- [Personnalisation de la plateforme](01-Description/08-Personnalisation-de-la-plateforme)

### Déploiement

- [Procédure d'accès au serveur de déploiement](02-Déploiement/01-Accès-au-serveur)
- [Procédure de déploiement](02-Déploiement/02-Procédure-de-déploiement)
- [Mise en place de l'environnement de développement conseillé](02-Déploiement/03-Environnement-de-développement)
- [Commandes utiles pour Docker](02-Déploiement/04-Commandes-utiles-Docker)
- [Commandes utiles pour la gestion de la base de données](02-Déploiement/05-Commandes-utiles-Postgres)

### Personnalisation

- [Initialisation d'objets nécessaires au fonctionnement de la plateforme](03-Personnalisation/01-Modèles,-paramètrages-et-contrôle-d'accès)