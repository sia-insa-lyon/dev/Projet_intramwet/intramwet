## Préliminaires

Pour installer python3 et pip, selon votre OS :

- Debian/Ubuntu : 
```bash
sudo apt install python3 python3-pip
```
- Windows : Un tutoriel [ici](https://www.howtogeek.com/197947/how-to-install-python-on-windows/)
- Mac : Un tutoriel [ici](http://docs.python-guide.org/en/latest/starting/install3/osx/)

Pour installer virtualenv, on utilise le gestionnaire de paquet de python :

```bash
pip3 install virtualenv
```

## Installation de Pycharm

Pycharm est des meilleurs IDE Python qui existent, avec support pour Django.

PyCharm est un IDE __payant__ de JetBrains, mais il est gratuit pour les étudiants. Pour bénéficier, 
voir [ici](https://www.jetbrains.com/student/). Ensuite, pour télécharger PyCharm allez 
[ici](https://www.jetbrains.com/pycharm/download/#section=linux). Il y a aussi un tuto d'installation 
(sous le gros logo).

Pendant l'installation, faire attention à cocher la case qui permet de créer les liens (symlink). L'installateur ne 
crée pas d'icône par défaut, il faut en demander une à la première ouverture de l'IDE en allant dans Tools - Create 
Desktop Entry.

## Mise en place du lanceur de Docker compose dans Pycharm

Pour l'instant, pour lancer le projet, vous devez manuellement faire un `docker-compose up` dans la console. Il
est possible de configurer Pycharm pour qu'il le fasse à notre place.

Il faut créer une nouvelle configuration de lancement. Pour cela, aller dans `Edit configurations`, et utiliser le 
petit plus vert en haut à gauche. De là, choisir `Docker` puis `Docker-compose`. Il faut ensuite spécifier le fichier 
`local.yml` à lancer en allant dans `Compose files`. 

Vous pouvez ensuite lancer cette configuration avec le petit triangle vert et l'arrêter avec le bouton rouge dans 
le tab Docker qui s'ouvre dans l'IDE quand vous lancez l'environnement.

## Mise en place de virtualenv

Pour permettre la correction/autocomplétion de code avec toutes les bibliothèques installées sur la site, Pycharm
doit avoir accès à toutes ces bibliothèques. La première méthode est d'installer sur votre machine tous les paquets nécessaires, avec `pip`. 
Pour éviter de polluer votre intépréteur python global et donc votre machine, vous pouvez aussi utiliser un 
interpréteur local interne à Pycharm. Pour cela, on utilise virtualenv, une machine virtuelle qui interagit avec
Pycharm. Ainsi, vous pourrez installer des bibliothèques depuis l'IDE dans virtualenv, et avoir directement 
l'autocomplétion, sans rien ajouter en plus sur votre machine.
Pour cela, il va falloir installer python3, pip (gestionnaire de bibliothèques python) et virtualenv.

## Mise en place du virtualenv dans Pycharm

Une fois le projet ouvert, il faut créer la machine virtuelle pour le déployer. Pour cela, aller dans fichier -> paramètres -> projet: intramwet -> interpréteur. Cliquer sur la barre de texte 'Project Interpreter' -> Show all, puis clicker sur le petit plus vert à droite pour créer la machine virtuelle. Assurez-vous que la machine virtuelle est créée avec Python 3.6 comme interpréteur (comme ça on a tous la même version), puis validez avec OK, et encore une fois OK pour choisir la nouvelle machine virtuelle choisie.

Maintenant que la machine virtuelle est créée, il faut y installer les bibliothèques. Normalement, Pycharm
détecte automatiquement le fichier `requirements.txt` contenant toutes les dépendances. En cliquant sur OK, un bandeau jaune devrait s'afficher en haut de la fenêtre du projet avec toutes les dépendances à installer. Cliquez sur `Install dependencies`. Validez.

Vous pouvez tester que l'installation fonctionne si le code dans les fichiers python du projet ne sont pas soulignés en rouge et que Pycharm vous propose les bonnes autocomplétions.

Voilà, c'est tout bon !

# Mise en place de Git

Nous vous conseillons d'utiliser un client Git plus puissant que Pycharm, tel que [**Gitkraken**](https://www.gitkraken.com/). Si vous en utilisez un, cette partie est **inutile**.

Pour git, c'est tout intégré dans l'IDE; vous pouvez directement faire les commits en appuyant sur l'icône Commit en 
haut a droite (ou bien avec le racourci Ctrl+K). Vous pouvez faire `Commit`, ou `Commit+Push` qui envoit directement 
vos modifications au serveur distant Gitlab. Vous pouvez mettre à jour votre projet depuis la branche principale avec 
`Update` (ou Ctrl+T).

Pour les Commits, faites-en le plus souvent possible (un commit est reversible, ça vous permet de revenir en arrière 
en cas de problèmes). Et puis ça aide à voire exactement ce qui a été changé depuis le dernier commit.

