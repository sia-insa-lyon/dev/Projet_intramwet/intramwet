```bash
docker-compose up
```
À utiliser depuis la racine du projet. Lance l'environnement de travail et le serveur. Vous pouvez ensuite l'arrêter 
avec `Ctrl+C`.
 
- `-d` : lance en mode détaché (sans retour en cas d'erreur, à utiliser en production).
- `--build` : recompile les images en lançant les conteneurs.
- `-f <file.yml>` : spécifie un fichier de configuration docker-compose à utiliser
(Par défaut, utilise `docker-compose.yml`).

Par défaut, l'accès au site en local se fait sur `https://127.0.0.1/`.

```bash
docker-compose stop
```

À utiliser depuis la racine du projet. Arrête les conteneurs décrits dans `docker-compose.yml`. Utile si vous avez 
lancé avec l'option `-d`.

```bash
docker-compose down
```
À utiliser depuis la racine du projet. Arrête et détruit tous les conteneurs.

```bash
docker-compose run web python /web/manage.py migrate
```

Execute `python /web/manage.py migrate` dans le conteneur `web` (donc celui de l'application Django).

```bash
docker-compose run web python /web/manage.py shell
```

Lance un shell Django dans le conteneur `web`. Utile pour débuguer. 

```bash
docker-compose build
```

Recompile les images Docker (en cas de modification).