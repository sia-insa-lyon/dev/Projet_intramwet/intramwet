## Préliminaires

### Installation de Docker

Il faut installer __Docker__ et __Docker compose__. 

Pour Windows, il suffit d'installer [Docker for Windows](https://docs.docker.com/docker-for-windows/install), qui installe tout ce qu'il faut.

Pour les distributions Linux basées sur Debian, il faut installer [Docker Community Edition](https://docs.docker.com/install/linux/docker-ce/debian/) et ensuite installer [Docker compose](https://docs.docker.com/compose/install/) (Attention, pour Linux Mint les comandes sont différentes !). Il faut ensuite se rajouter au groupe `docker` et relancer son ordi:

```bash
sudo adduser nom docker
```

### Paramétrage

Avant toute chose, il faut créer les fichiers `db.env` et `django.env` dans le dossier `env`. Ces fichiers définissent un certain nombre de variables d'environnement pour le déploiement. Pour les créer, utilisez les fichiers-exemple fournis.

Le fichier `db.env` définit les paramètres pour la base de données. **Il est primordial de modifier le mot de passe de la base de données avant de déployer en production (en modifiant la variable `DB_PASS`).**

Le fichier `django.env` définit des paramètres pour Django. Il faut notamment définir le mot de passe pour le compte mail, les Webhooks pour les différents channels Discord, et le DSN Sentry.

Pour créer les webhooks nécessaires au bot Discord, regardez [ce tutoriel](https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks). Pour l'intégration Sentry, regardez [ce tutoriel](https://docs.sentry.io/platforms/python/django/).

## Déploiement

Si vous déployez une configuration qui utilise Let's Encrypt pour la première fois, lancez le script `init-letsencrypt.sh`. Plus de détails dans [ce tutoriel](https://medium.com/@pentacent/nginx-and-lets-encrypt-with-docker-in-less-than-5-minutes-b4b8a60d3a71).

Pour toutes les configurations, utilisez la commande suivante pour déployer :

```bash
docker-compose up
```

## Initialisation de la base de données

Pour initialiser la base de données (groupes, catégories, permissions, etc.), utilisez la commande suivante :

```bash
docker-compose run --rm web python manage.py init
``` 

## Mise à jour

Pour mettre à jour un serveur local, utilisez la commande suivante :

```bash
docker-compose up --build
```

Pour les autres :
```bash
docker-compose pull
docker-compose up
```