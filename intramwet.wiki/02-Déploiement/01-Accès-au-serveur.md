La VM est configurée avec 2 comptes:
- `intramwet` - compte root avec tous les accès sur le serveur (via `sudo`)
- `web` - compte normal sans accès root sur lequel tourne le site

L'idée générale est de protéger au maximum le serveur en déployant le site sur un compte utilisteur sans droits.

## Accès par SSH

L'accès au serveur se fait par SSH, que ce soit pour le compte `intramwet` ou le compte `web`. Ceci permet de renforcer la sécurité du serveur: pour y accéder, on a besoin d'un truc qu'on a (la clé SSH privée) et d'un truc qu'on connait (le mot de passe de la clé SSH).

### Préliminaires

Il faut installer, si vous ne l'avez pas sur votre machine, un client ssh. Le plus pratique est d'utiliser `openssh-client` (sur Linux ou le Bash Linux sur Windows).

### Qu'est-ce qu'une clé SSH

Une clé SSH permet l'authentification de votre machine. Elle est composée de 2 parties:
- une clé publique que vous partagez; elle se présente sous la forme d'un fichier `<nom de la clé>.pub`
- une clé privée qui **reste sur votre machine**; elle se présente sous la forme d'une fichier `<nom de la clé>`

Pour plus de détails, voir [ici](https://www.ssh.com/ssh/key/).

Par la suite, je suppose que la clé privée va s'appeller `intramwet` et la clé publique `intramwet.pub`. Les clés seront générées dans le dossier par défaut (`~/.ssh`).

### Créer une nouvelle clé

Pour créer une nouvelle clé, sur votre machine:

```bash
ssh-keygen -t ed25519 -f ~/.ssh/intramwet
```

`ssh-keygen` va vous demander un mot de passe. Bien que pas obligatoire, c'est une vraiment mauvaise idée de créer une clé sans mot de passe (en cas de perte/vol un mot de passe rend la clé effectivement inutilisable, sauf si votre mot de passe est _password_). 

Ensuite, il faut authoriser cette clé côté serveur. Pour cela, il faut se connecter au serveur via une machine qui est déjà authorisée et rajouter votre clé **publique** dans le fichier `~/.ssh/authorized_keys` pour chaque compte auquel vous voulez accéder.

Pour obtenir votre clé publique :

```bash
cat ~/.ssh/intramwet.pub
```

### Mise en place du raccourci de connexion

La connexion via SSH peut se faire via la commande :

```bash
ssh -i ~/.ssh/intramwet [intramwet|web]@medialamouette.fr
```

C'est un peu long, et au bout de la 10ème connexion vous en aurez marre. Pour créer un raccourci, il faut créer un fichier `~/.ssh/config` sur votre machine personnelle. Son contenu est le suivant :

```bash
Host mwet
        Hostname medialamouette.fr
        User web
        IdentityFile ~/.ssh/intramwet

Host mwet-su
        Hostname medialamouette.fr
        User intramwet
        IdentityFile ~/.ssh/intramwet
```

Il faut ensuite donner les droits de lecture et d'écriture au fichier `~/.ssh/config` :

```bash
chmod 600 ~/.ssh/config
```

Ensuite vous pourrez vous connectez avec :

```bash
ssh mwet     # compte normal
ssh mwet-su  # compte root
```

## Références

[Tutoriel de génération de clés de GitLab](https://docs.gitlab.com/ee/ssh/README.html#ed25519-ssh-keys)
