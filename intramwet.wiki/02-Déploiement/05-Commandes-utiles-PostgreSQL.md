La base de données utilise le moteur PostgresSQL.

## Sauvegardes

Les sauvegardes sont effectuées quotidiennement à l'aide d'un job cron. Elles sont stockées dans `postgres/backups/`.

Pour restaurer une backup dans la BD :

```bash
sh postgres/restore.sh docker-compose.yml postgres/.env /postgres/backups/<filename>
``` 

## Suppression

En cas de conflit de migrations avec une base de données incohérente, vous pouvez la réinitialiser avec les commandes suivantes :

```bash
# On supprime les conteneurs
docker-compose down 

# On supprime les volumes qui sauvegardent la BD
docker volume prune -f

# On recrée les conteneurs (le --build est optionnel et permet de recompiler les conteneurs)
docker-compose up -d --build 

# On applique les migrations
docker-compose run web python /web/manage.py migrate 

# On collecte les fichiers statiques (images, js, etc, pour NGINX)
docker-compose run web python /web/manage.py collectstatic 

# On supprime tous les conteneurs, pour enlever les conteneurs créés par les 2 commandes précédentes
docker-compose down 
```
