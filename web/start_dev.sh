#!/bin/bash

set -o errexit
set -o pipefail
set -o nounset

python /app/manage.py collectstatic --noinput
python /app/manage.py migrate --noinput

gunicorn intramwet.wsgi:application \
    --bind 0.0.0.0:8000 \
    --workers 2 \
    --log-level=info \
    --reload \
    --chdir=/app
