from django.urls import path, include

urlpatterns = [
    path('', include('intramwet.accueil.urls')),
    path('projet/', include('apps.projet.urls')),
    path('utilisateur/', include('apps.utilisateur.urls')),
    path('materiel/', include('apps.materiel.urls')),
    path('treso/', include('apps.treso.urls')),
    path('documentation/', include('apps.documentation.urls')),
    path('passation/', include('apps.passation.urls')),
    path('conf/', include('apps.conf.urls')),
]
