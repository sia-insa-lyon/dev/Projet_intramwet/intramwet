from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'intramwet.core'
