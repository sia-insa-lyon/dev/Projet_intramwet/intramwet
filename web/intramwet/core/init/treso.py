from apps.initializer.base_initializer import BaseInitializer
from apps.initializer.initialization_handler import initialization_handler
from apps.treso.models import CategorieGrilleTarifaire


class TresoInitializer(BaseInitializer):
    priority = 1

    def init_categories_grille(self):
        CategorieGrilleTarifaire.objects.create(nom='Matériel')
        CategorieGrilleTarifaire.objects.create(nom='Montage')
        CategorieGrilleTarifaire.objects.create(nom='Tournage')
        CategorieGrilleTarifaire.objects.create(nom='Autres')
