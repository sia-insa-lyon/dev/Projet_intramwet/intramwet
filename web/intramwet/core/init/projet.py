from django.contrib.auth.models import Group

from apps.initializer.base_initializer import BaseInitializer
from apps.initializer.initialization_handler import initialization_handler
from apps.projet.models import TypeProjet, TypeParticipantProjet, PreconfigurationTache


class ProjetInitializer(BaseInitializer):
    priority = 4

    def init_type_projet(self):
        cm, _ = TypeProjet.objects.get_or_create(typeProjet="Court-Métrage")

        docu, _ = TypeProjet.objects.get_or_create(typeProjet="Documentaire")

        capta, _ = TypeProjet.objects.get_or_create(typeProjet="Captation")

        event, _ = TypeProjet.objects.get_or_create(typeProjet="Evènement")

        autre, _ = TypeProjet.objects.get_or_create(typeProjet="Autre")

        formation, _ = TypeProjet.objects.get_or_create(typeProjet="Formation")

        aftermovie, _ = TypeProjet.objects.get_or_create(
            typeProjet="Aftermovie")

        clip, _ = TypeProjet.objects.get_or_create(typeProjet="Clip")

        TypeProjet.objects.get_or_create(typeProjet="Bural")

        formation_privee, _ = TypeProjet.objects.get_or_create(
            typeProjet="Formation privée")

        p_cm = cm.get_permission().id
        p_docu = docu.get_permission().id
        p_capta = capta.get_permission().id
        p_event = event.get_permission().id
        p_autre = autre.get_permission().id
        p_formation = formation.get_permission().id
        p_aftermovie = aftermovie.get_permission().id
        p_clip = clip.get_permission().id
        p_formation_privee = formation_privee.get_permission().id

        membreVA = Group.objects.get(name="Membre VA/INSA")
        membreVA.permissions.add(p_cm, p_docu, p_capta, p_event, p_autre,
                               p_formation, p_aftermovie, p_clip,
                               p_formation_privee)
        membreVA.save()

        membreAncienINSA = Group.objects.get(name="Membre Ancien INSA")
        membreAncienINSA.permissions.add(p_cm, p_docu, p_capta, p_event, p_autre,
                               p_formation, p_aftermovie, p_clip,
                               p_formation_privee)
        membreAncienINSA.save()

        membreExterne = Group.objects.get(name="Membre Externe")
        membreExterne.permissions.add(p_cm, p_docu, p_capta, p_event, p_autre,
                               p_formation, p_aftermovie, p_clip,
                               p_formation_privee)
        membreExterne.save()

        exterieur = Group.objects.get(name="Extérieur")
        exterieur.permissions.add(p_capta, p_event, p_clip, p_aftermovie)
        exterieur.save()

        association = Group.objects.get(name="Association")
        association.permissions.add(p_docu, p_capta, p_event, p_autre,
                                    p_formation,
                                    p_aftermovie, p_clip)
        association.save()

        capsules_videos = Group.objects.get(name="Capsules Vidéos")
        capsules_videos.permissions.add(p_formation)
        capsules_videos.save()

        cinet = Group.objects.get(name="Cinéma-études")
        cinet.permissions.add(p_cm, p_docu, p_capta, p_event, p_autre,
                              p_formation,
                              p_aftermovie, p_clip,
                              p_formation_privee)
        cinet.save()

    def init_type_participant_projet(self):
        for t in ["Réalisateur", "Caméraman", "Monteur", "Ingénieur Son",
                  "Scénariste", "Directeur Photo", "Acteur", "Support"]:
            TypeParticipantProjet.objects.get_or_create(typeParticipant=t)

    def init_preconfiguration_tache(self):
        PreconfigurationTache.objects.get_or_create(nom="Tournage capta",
                                                    nomTache="Captation",
                                                    visible=True,
                                                    ficheCessionDroits=True,
                                                    notifPourInscription=True,
                                                    notifBureauTermine=True,
                                                    notifBureauAvantDate=True,
                                                    notifMembreAvantDate=True,
                                                    notifMembreApresDate=True,
                                                    notifBureau=False,
                                                    notifMembre=False)
        PreconfigurationTache.objects.get_or_create(nom="Montage capta",
                                                    nomTache="Montage",
                                                    visible=True,
                                                    ficheCessionDroits=True,
                                                    notifPourInscription=True,
                                                    notifBureauTermine=True,
                                                    notifBureauAvantDate=False,
                                                    notifMembreAvantDate=False,
                                                    notifMembreApresDate=False,
                                                    notifBureau=True,
                                                    notifMembre=True)
        PreconfigurationTache.objects.get_or_create(nom="Administration",
                                                    visible=False,
                                                    ficheCessionDroits=False,
                                                    notifPourInscription=False,
                                                    notifBureauTermine=False,
                                                    notifBureauAvantDate=False,
                                                    notifMembreAvantDate=False,
                                                    notifMembreApresDate=False,
                                                    notifBureau=True,
                                                    notifMembre=False)
        PreconfigurationTache.objects.get_or_create(nom="Projet personnel",
                                                    visible=True,
                                                    ficheCessionDroits=False,
                                                    notifPourInscription=True,
                                                    notifBureauTermine=False,
                                                    notifBureauAvantDate=False,
                                                    notifMembreAvantDate=True,
                                                    notifMembreApresDate=False,
                                                    notifBureau=False,
                                                    notifMembre=True)
