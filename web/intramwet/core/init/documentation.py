from django.contrib.auth.models import Group

from apps.documentation.models import Categorie
from apps.initializer.base_initializer import BaseInitializer


class DocumentationInitializer(BaseInitializer):
    priority = 3

    def init_categories_documents(self):
        Categorie.objects.get_or_create(nom="Bible Bureau")

        guide_membre, _ = Categorie.objects.get_or_create(
            nom="Guide du membre")
        guide_membre.save()
        p_guide_membre = guide_membre.get_permission().id

        bible_technique, _ = Categorie.objects.get_or_create(
            nom="Bible technique")
        bible_technique.save()
        p_bible_technique = bible_technique.get_permission().id

        guide_exterieur, _ = Categorie.objects.get_or_create(
            nom="Guide d'utilisation extérieur")
        guide_exterieur.save()
        p_guide_exterieur = guide_exterieur.get_permission().id

        guide_association, _ = Categorie.objects.get_or_create(
            nom="Guide d'utilisation association")
        guide_association.save()
        p_guide_asso = guide_association.get_permission().id

        guide_capsule, _ = Categorie.objects.get_or_create(
            nom="Guide d'utilisation Capsules Vidéos")
        guide_capsule.save()
        p_guide_capsule = guide_capsule.get_permission().id

        membreVA = Group.objects.get(name="Membre VA/INSA")
        membreVA.permissions.add(p_bible_technique, p_guide_membre)
        membreVA.save()

        membreAncienINSA = Group.objects.get(name="Membre Ancien INSA")
        membreAncienINSA.permissions.add(p_bible_technique, p_guide_membre)
        membreAncienINSA.save()

        membreExterne = Group.objects.get(name="Membre Externe")
        membreExterne.permissions.add(p_bible_technique, p_guide_membre)
        membreExterne.save()

        exterieur = Group.objects.get(name="Extérieur")
        exterieur.permissions.add(p_guide_exterieur)
        exterieur.save()

        association = Group.objects.get(name="Association")
        association.permissions.add(p_guide_asso)
        association.save()

        capsules_videos = Group.objects.get(name="Capsules Vidéos")
        capsules_videos.permissions.add(p_guide_capsule)
        capsules_videos.save()

        cinet = Group.objects.get(name="Cinéma-études")
        cinet.permissions.add(p_bible_technique, p_guide_membre)
        cinet.save()
