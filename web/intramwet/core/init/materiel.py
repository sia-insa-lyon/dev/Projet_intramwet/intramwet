from django.contrib.auth.models import Group

from apps.initializer.base_initializer import BaseInitializer
from apps.materiel.models import TypeMateriel, ProprietaireMateriel


class MaterielInitializer(BaseInitializer):
    priority = 5

    def init_type_materiel(self):
        for m in ["Boitier", "Son", "Accessoires", "GoPro", "Camera",
                  "Objectif",
                  "Lumière", "Batterie", "Cartes mémoires", "Connectique",
                  "Sacoche"]:
            TypeMateriel.objects.get_or_create(typeMateriel=m)

    def init_proprietaire_materiel(self):
        matos_mouette, _ = ProprietaireMateriel.objects.get_or_create(
            nomProprietaire="Média La Mouette")
        p_emprunt_gratuit_matos_mouette = matos_mouette.get_emprunt_gratuit_permission().id
        p_location_matos_mouette = matos_mouette.get_emprunt_payant_permission().id

        matos_cva, _ = ProprietaireMateriel.objects.get_or_create(
            nomProprietaire="CVA")
        p_emprunt_gratuit_matos_cva = matos_cva.get_emprunt_gratuit_permission().id

        matos_alma, _ = ProprietaireMateriel.objects.get_or_create(
            nomProprietaire="ALMA")
        p_emprunt_gratuit_matos_alma = matos_alma.get_emprunt_gratuit_permission().id

        matos_cinet, _ = ProprietaireMateriel.objects.get_or_create(
            nomProprietaire="Cinéma-Études")
        p_emprunt_gratuit_matos_cinet = matos_cinet.get_emprunt_gratuit_permission().id

        membreVA = Group.objects.get(name="Membre VA/INSA")
        membreVA.permissions.add(p_emprunt_gratuit_matos_mouette,
                               p_emprunt_gratuit_matos_cva,
                               p_emprunt_gratuit_matos_alma,
                               p_emprunt_gratuit_matos_cinet)
        membreVA.save()

        membreAncienINSA = Group.objects.get(name="Membre Ancien INSA")
        membreAncienINSA.permissions.add(p_emprunt_gratuit_matos_mouette,
                               p_emprunt_gratuit_matos_cva,
                               p_emprunt_gratuit_matos_alma,
                               p_emprunt_gratuit_matos_cinet)
        membreAncienINSA.save()

        membreExterne = Group.objects.get(name="Membre Externe")
        membreExterne.permissions.add(p_emprunt_gratuit_matos_mouette,
                               p_emprunt_gratuit_matos_cva,
                               p_emprunt_gratuit_matos_alma,
                               p_emprunt_gratuit_matos_cinet)
        membreExterne.save()

        exterieur = Group.objects.get(name="Extérieur")
        exterieur.permissions.add(p_location_matos_mouette)
        exterieur.save()

        association = Group.objects.get(name="Association")
        association.permissions.add(p_emprunt_gratuit_matos_cva,
                                    p_location_matos_mouette)
        association.save()

        capsules_videos = Group.objects.get(name="Capsules Vidéos")
        capsules_videos.permissions.add(p_emprunt_gratuit_matos_alma,
                                        p_location_matos_mouette)
        capsules_videos.save()

        cinet = Group.objects.get(name="Cinéma-études")
        cinet.permissions.add(p_emprunt_gratuit_matos_mouette,
                              p_emprunt_gratuit_matos_cva,
                              p_emprunt_gratuit_matos_alma,
                              p_emprunt_gratuit_matos_cinet)
        cinet.save()
