from datetime import timedelta, date

from django.contrib.auth import get_user_model
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.clickjacking import xframe_options_exempt

from apps.conf.context_processors import intramwet_settings
from apps.materiel.models import EmpruntLocal, EmpruntMateriel, EmpruntElement
from apps.projet.models import Projet, ParticipantProjet
from apps.treso.models import NoteDeFrais


def accueil(request, modal_inscription: bool = False,
            modal_emprunt_reussi: bool = False,
            modal_nom_utilisateur: bool = False) -> HttpResponse:
    """ Point d'accès à l'accueil du site

    :param request:
    :param modal_inscription: si on doit afficher la modale d'inscription
    :param modal_emprunt_reussi: si on doit afficher la modale d'emprunt réussi
    :param modal_nom_utilisateur: si on doit afficher la modale d'oubli de nom d'utilisateur
    :return:
    """
    if not request.user or not request.user.is_authenticated:
        return accueil_static(request, modal_inscription,
                              modal_nom_utilisateur)

    return accueil_utilisateur(request, modal_emprunt_reussi)


def accueil_utilisateur(request, modal_emprunt_reussi: bool) -> HttpResponse:
    """ Interface d'accueil pour les utilisateurs connectés

    :param request:
    :param modal_inscription: si on doit afficher la modale d'inscription
    :param modal_emprunt_reussi: si on doit afficher la modale d'emprunt réussi
    :param modal_nom_utilisateur: si on doit afficher la modale d'oubli de nom d'utilisateur
    :return:
    """
    if request.user.has_perm('utilisateur.acceder_local'):
        emprunts_local = EmpruntLocal.objects.filter(
            Q(statut=EmpruntLocal.VALIDE), Q(debut__gte=date.today()) & Q(
                debut__lte=date.today() + timedelta(days=7)))

    if request.user.has_perm('utilisateur.acceder_materiel'):
        emprunts_materiel = EmpruntMateriel.objects.filter(
            Q(membre=request.user), Q(debut__gte=date.today()) &
                                    Q(fin__lte=date.today() + timedelta(
                                        days=7)))
        emprunt_elements = []
        for emprunt in emprunts_materiel:
            emprunt_elements += EmpruntElement.objects.filter(
                Q(emprunt=emprunt), ~Q(statut=EmpruntElement.RENDU))

    if request.user.has_perm('utilisateur.acceder_projets'):
        projets_en_cours = Projet.objects.accessibles(
            utilisateur=request.user).filter(
            deadlineRendu__gte=date.today(), statut=Projet.VALIDE)
        projets_membre = []
        for projet in projets_en_cours:
            if ParticipantProjet.objects.filter(projet=projet,
                                                utilisateur=request.user).exists():
                projets_membre.append(projet)

    if request.user.is_staff:
        depart_emprunt = EmpruntElement.objects.filter(
            emprunt__debut__date=date.today(),
            statut=EmpruntElement.VALIDE).order_by('emprunt',
                                                   'emprunt__debut')
        retour_emprunt = EmpruntElement.objects.filter(
            emprunt__fin__date=date.today(),
            statut=EmpruntElement.VALIDE).order_by('emprunt', 'emprunt__fin')
        users_a_valider = get_user_model().objects.filter(valide=False,
                                                     is_superuser=False,
                                                     is_active=True,
                                                     actif=True)
        users_a_valider_cotisation = get_user_model().objects.filter(
            statutCotisation=get_user_model().NON_PAYEE, actif=True)
        ndfs_en_attente = NoteDeFrais.objects.filter(statut=NoteDeFrais.EnumStatut.ATTENTE).order_by('-numero')

    return render(request, "accueil.html", locals())


@xframe_options_exempt
def accueil_static(request, modal_inscription: bool,
                   modal_nom_utilisateur: bool) -> HttpResponse:
    """ Interface d'accueil statique, site vitrine de l'association

    :param request:
    :param modal_inscription: si on doit afficher la modale d'inscription
    :param modal_nom_utilisateur: si on doit afficher la modale d'oubli du nom d'utilisateur
    :return:
    """
    props = intramwet_settings()
    if props['INTRAMWET_HOMEPAGE_ACTIVATED']:
        return render(request, "accueil_static.html", locals())
    return redirect('connexion')
