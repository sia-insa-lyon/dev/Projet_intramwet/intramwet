#!/bin/bash

# Usage : ./test_rq.sh

test $(rq info -u redis://redis:6379 high default low | grep 'workers' | cut -d' ' -f 1) -gt 0