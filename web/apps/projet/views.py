import json
from datetime import datetime, timezone

from django.contrib.auth.models import Permission
from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.http import HttpResponse, Http404, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from apps.conf.utils import media_response
from apps.conf.bot import Bot, Email
from .decorators import can_manage_projet, can_access_projet
from .forms import ProjetForm, TacheForm, CreneauForm, TypeProjetForm
from .models import Projet, ParticipantProjet, Tache, Creneau, \
    TypeParticipantProjet, FichierTache, \
    PreconfigurationTache, TypeProjet
from apps.treso.views import save_pdf
from apps.utilisateur.models import StatutCA
from apps.passation.decorators import can_access_passation, register
from .utils import notify_new_projet


@can_manage_projet
def manage_types_projet(request):
    types_projet = TypeProjet.objects.all().order_by('typeProjet')
    if request.method == "POST":
        form = TypeProjetForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = TypeProjetForm()
    return render(request, 'manage_type_projet.html', locals())


@can_manage_projet
def delete_type_projet(request, id: int):
    TypeProjet.objects.get(id=id).delete()
    return redirect('gestion_types_projet')


@can_manage_projet
def edit_type_projet(request, id: int):
    type = TypeProjet.objects.get(id=id)
    if request.method == "POST":
        form = TypeProjetForm(request.POST, instance=type)
        if form.is_valid():
            form.save()
            return JsonResponse({'success': True})
        return JsonResponse({'success': False})
    form = TypeProjetForm(instance=type)
    return render(request, 'edit_type_projet.html', locals())


@can_manage_projet
def valider_projet(request, id: int):
    projet = get_object_or_404(Projet, id=id)
    projet.statut = Projet.VALIDE
    projet.save()
    if ParticipantProjet.objects.filter(admin=True, projet=projet).exists():
        users = ParticipantProjet.objects.filter(admin=True, projet=projet)
        Email('emails/projet_valide.html', data=locals()).send_mass(users.values_list('utilisateur__email', flat=True))
    return redirect('projet', id=id)


@can_access_projet
def projet_liste(request):
    projets_en_attente = Projet.objects.accessibles(request.user).filter(
        statut=Projet.EN_ATTENTE)
    if not request.user.is_staff:
        for projet in projets_en_attente:
            if not projet.est_admin(request.user):
                projets_en_attente = projets_en_attente.exclude(id=projet.id)
    projets_en_attente = projets_en_attente.distinct()
    projets_en_cours = Projet.objects.accessibles(request.user).filter(
        statut=Projet.VALIDE)
    projets_finis = Projet.objects.accessibles(request.user).filter(
        statut=Projet.FINI)
    projets_admin = Projet.objects.accessibles(request.user).filter(
        participants__admin=True,
        participants__utilisateur=request.user)
    return render(request, 'liste_projet.html', locals())


@can_manage_projet
def statut_projet(request):
    if request.method == 'POST':
        try:
            projet = Projet.objects.get(id=request.POST['id_projet'])
            statut = int(request.POST['valide'])

            if statut == 0:
                projet.delete()
            elif statut == 1:
                projet.statut = Projet.VALIDE
                projet.save()
                if ParticipantProjet.objects.filter(admin=True,
                                                    projet=projet).exists():
                    users = ParticipantProjet.objects.filter(admin=True,
                                                             projet=projet)
                    Email('emails/projet_valide.html',
                          data=locals()).send_mass(
                        users.values_list('utilisateur__email', flat=True))
            elif statut == 2:
                projet.statut = Projet.FINI
                projet.save()
            else:
                return JsonResponse(
                    {'erreur': "Contactez l'administrateur du site !"})
        except Projet.DoesNotExist:
            return JsonResponse({
                'erreur': "Le projet n'existe pas (id={}) !".format(
                    request.POST['id_projet'])})

    return JsonResponse({'success': True})


@can_access_projet
def projet(request, id_projet):
    try:
        projet = Projet.objects.accessibles(request.user).get(id=id_projet)
    except Projet.DoesNotExist:
        raise Http404

    types_participants = TypeParticipantProjet.objects.all()

    participants = ParticipantProjet.objects.filter(projet=projet)
    participants_mouette = participants.filter(
        utilisateur__groups__name='Membre')
    participants_exterieur = participants.exclude(
        utilisateur__groups__name='Membre')

    admins = participants.filter(admin=True)
    est_admin = participants.filter(utilisateur=request.user,
                                    admin=True).exists() or request.user.is_staff

    if est_admin:
        taches = Tache.objects.filter(projet=projet)
    else:
        taches = Tache.objects.filter(projet=projet, visible=True)

    creneaux_inscrits = Creneau.objects.filter(tache__in=taches,
                                               membreAssigne__utilisateur=request.user)
    est_inscrit_au_projet = request.user in get_user_model().objects.filter(
        id__in=participants.values('utilisateur__id'))

    return render(request, 'projet.html', locals())


@can_access_projet
def tache_details(request):
    if request.method == 'POST':
        try:
            tache = Tache.objects.get(id=request.POST['id_tache'])
            participants = ParticipantProjet.objects.filter(
                projet=tache.projet)
            est_admin = participants.filter(utilisateur=request.user,
                                            admin=True).exists() or request.user.is_staff
            if est_admin:
                taches = Tache.objects.filter(projet=tache.projet)
            else:
                taches = Tache.objects.filter(projet=tache.projet,
                                              visible=True)
            creneaux_inscrits = Creneau.objects.filter(tache__in=taches,
                                                       membreAssigne__utilisateur=request.user)
            est_inscrit_au_projet = request.user in get_user_model().objects.filter(
                id__in=participants.values('utilisateur__id'))
            est_inscrit_tache = tache.est_inscrit(request.user)
            return render(request, 'projet_tache.html', locals())
        except Tache.DoesNotExist:
            raise PermissionDenied
    return HttpResponse('error')


@can_access_projet
def projet_description(request):
    if request.method == 'POST':
        try:
            projet = Projet.objects.accessibles(request.user).get(
                id=request.POST['id_projet'])
            participants = ParticipantProjet.objects.filter(projet=projet)
            participants_mouette = participants.filter(
                utilisateur__groups__name='Membre')
            participants_exterieur = participants.exclude(
                utilisateur__groups__name='Membre')
            admins = participants.filter(admin=True)
            est_admin = participants.filter(utilisateur=request.user,
                                            admin=True).exists() or request.user.is_staff
            types_participants = TypeParticipantProjet.objects.all()
            est_inscrit_au_projet = request.user in get_user_model().objects.filter(
                id__in=participants.values('utilisateur__id'))
            return render(request, 'projet_description.html', locals())
        except Projet.DoesNotExist:
            return HttpResponse('error')
    return HttpResponse('error')


@can_access_projet
def diffuser_tache(request):
    if request.method == 'POST':
        try:
            tache = Tache.objects.get(id=request.POST['id_tache'])
            if not tache.diffusee and tache.projet.est_admin(request.user):
                tache.diffusee = True
                tache.save()
                Bot('diffusion_tache', data=locals()).send()
                perm = Permission.objects.get(codename='fiche_inscription')
                users = get_user_model().objects.filter(
                    Q(groups__permissions=perm) | Q(
                        user_permissions=perm)).distinct()
                Email('emails/diffusion_tache.html',
                      data={'tache': tache}).send_mass(
                    users.values_list('email', flat=True))
        except Tache.DoesNotExist:
            return HttpResponse('error')
    return JsonResponse({})


@can_access_projet
def ajout_participant_projet(request):
    if request.method == 'POST':
        try:
            user = get_user_model().objects.get(id=request.POST['id_user'])
            projet = Projet.objects.accessibles(request.user).get(
                id=request.POST['id_projet'])
            if ParticipantProjet.objects.filter(projet=projet,
                                                utilisateur=user).exists():
                return JsonResponse(
                    {'erreur': "L'utilisateur est déjà inscrit au projet !"})

            participant = ParticipantProjet(projet=projet, utilisateur=user)
            participant.save()
            for id_role in json.loads(request.POST['typeParticipant']):
                participant.typeParticipant.add(
                    TypeParticipantProjet.objects.get(id=int(id_role)))

        except get_user_model().DoesNotExist:
            return JsonResponse({
                'erreur': "L'utilisateur n'existe pas (id={}) !".format(
                    request.POST['id_user'])})
        except Projet.DoesNotExist:
            return JsonResponse({
                'erreur': "Le projet n'existe pas (id={}) !".format(
                    request.POST['id_projet'])})
        except TypeParticipantProjet.DoesNotExist:
            return JsonResponse({
                'erreur': "Le rôle n'existe pas (id={}) !".format(
                    request.POST['typeParticipant'])})

    return JsonResponse({'success': True})


@can_access_projet
def ajout_participant_tache(request):
    if request.method == 'POST':
        try:
            user = get_user_model().objects.get(id=request.POST['id_user'])
            tache = Tache.objects.get(id=request.POST['id_tache'])
            participant = ParticipantProjet.objects.get(projet=tache.projet,
                                                        utilisateur=user)
            tache.membreAssigne.add(participant)
        except get_user_model().DoesNotExist:
            return JsonResponse({
                'erreur': "L'utilisateur n'existe pas (id={}) !".format(
                    request.POST['id_user'])})
        except Tache.DoesNotExist:
            return JsonResponse({
                'erreur': "Le tâche n'existe pas (id={}) !".format(
                    request.POST['id_tache'])})
        except ParticipantProjet.DoesNotExist:
            return JsonResponse(
                {'erreur': "Veuillez vous inscrire d'abord au projet !"})

        if tache.ficheCessionDroits and tache.necessite_cession_droits(user):
            try:
                president = \
                    get_user_model().objects.filter(
                        statutCA__poste=StatutCA.PRESIDENT)[
                        0]
                today = datetime.now(timezone.utc)
                filename = "CD_{}_{}{}".format(
                    tache.projet.nom.__repr__().replace(' ', '_'),
                    user.first_name.capitalize()[0],
                    user.last_name.capitalize())
                pdf = save_pdf('fiche_cession_droits.rml', locals())
                fichier = FichierTache(tache=tache, utilisateur=user)
                fichier.fichier.save("%s.pdf" % filename, pdf)
                fichier.save()
            except IndexError:
                Bot('erreur_pas_president', Bot.CHAN_BUREAU, locals()).send()

    return JsonResponse({'success': True})


@can_access_projet
def ajout_participant_creneau(request):
    if request.method == 'POST':
        try:
            user = get_user_model().objects.get(id=request.POST['id_user'])
            creneau = Creneau.objects.get(id=request.POST['id_creneau'])
            participant = ParticipantProjet.objects.get(
                projet=creneau.tache.projet, utilisateur=user)
            creneau.membreAssigne.add(participant)
        except get_user_model().DoesNotExist:
            return JsonResponse({
                'erreur': "L'utilisateur n'existe pas (id={}) !".format(
                    request.POST['id_user'])})
        except Creneau.DoesNotExist:
            return JsonResponse({
                'erreur': "Le créneau n'existe pas (id={}) !".format(
                    request.POST['id_creneau'])})
        except ParticipantProjet.DoesNotExist:
            return JsonResponse(
                {'erreur': "Veuillez vous inscrire d'abord au projet !"})

        if creneau.tache.ficheCessionDroits and creneau.tache.necessite_cession_droits(
                user):
            today = datetime.now(timezone.utc)
            fichier = FichierTache(tache=creneau.tache, utilisateur=user)
            fichier.save()
            fichier.fichier.save(fichier.filename,
                                 save_pdf('fiche_cession_droits.rml',
                                          locals()))

    return JsonResponse({'success': True})


@can_access_projet
def ajout_projet(request):
    if request.method == 'POST':
        form = ProjetForm(request.POST)
        if form.is_valid():
            projet = form.save()
            Bot('projet', Bot.CHAN_BUREAU, data=locals()).send()
            Email('emails/projet.html', request.user.email, locals()).send()
            notify_new_projet(request, projet)
            if not request.user.is_staff:
                admin = ParticipantProjet(projet=projet,
                                          utilisateur=request.user, admin=True)
                admin.save()
                real = TypeParticipantProjet.objects.get(pk=1)
                admin.typeParticipant.add(real)
            return redirect('projet_liste')
    else:
        form = ProjetForm()
    edit = False
    return render(request, 'edit_projet.html', locals())


@can_access_projet
def edit_projet(request, id_projet):
    try:
        projet = Projet.objects.accessibles(request.user).get(id=id_projet)
        if not projet.est_admin(request.user):
            raise PermissionDenied
    except Projet.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        form = ProjetForm(request.POST, instance=projet)
        if form.is_valid():
            projet = form.save()
            print(form.cleaned_data['participants'])
            for id_membre in [e for e in projet.participants.all().values_list(
                    'utilisateur__id', flat=True)
                              if e not in [int(e['nom']) for e in
                                           form.cleaned_data['participants']]]:
                # Inscrit à supprimer
                print("Suppression de {}".format(id_membre))
                for e in projet.participants.filter(utilisateur__id=id_membre):
                    print(e)
                    e.delete()

            for participant in form.cleaned_data['participants']:
                id_membre = int(participant['nom'])

                print("Checking for id = {}....".format(id_membre))
                print("Déjà inscrit : {}".format(
                    projet.participants.all().values_list('utilisateur__id',
                                                          flat=True)))
                if id_membre in projet.participants.all().values_list(
                        'utilisateur__id', flat=True):
                    # Modification du rôle
                    part = ParticipantProjet.objects.filter(projet=projet,
                                                            utilisateur__id=id_membre)[
                        0]
                    part.admin = participant['admin']
                    part.typeParticipant.clear()
                    part.save()
                else:
                    # Nouvel inscrit
                    part = ParticipantProjet(projet=projet,
                                             utilisateur=get_user_model().objects.get(
                                                 id=id_membre),
                                             admin=participant['admin'])
                    part.save()
                for role in participant['roles']:
                    part.typeParticipant.add(
                        TypeParticipantProjet.objects.get(id=int(role)))
            return redirect('projet', id_projet)
    else:
        form = ProjetForm(instance=projet)

    perm = Permission.objects.get(codename='acceder_projets')
    membres = get_user_model().objects.filter(Q(groups__permissions=perm) | Q(
        user_permissions=perm)).distinct().order_by(
        'first_name')
    types = TypeParticipantProjet.objects.all()
    edit = True
    return render(request, 'edit_projet.html', locals())


@can_access_projet
def ajout_tache(request, id_projet):
    try:
        projet = Projet.objects.accessibles(request.user).get(id=id_projet)
        if not projet.est_admin(request.user):
            raise PermissionDenied
    except Projet.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        form = TacheForm(request.POST)
        if form.is_valid():
            tache = form.save(commit=False)
            tache.projet = projet
            tache.save()
            return redirect('projet', id_projet)
    else:
        form = TacheForm()
    edit = False
    preconfigurations = PreconfigurationTache.objects.all()
    return render(request, 'edit_tache.html', locals())


@can_access_projet
def edit_tache(request, id_tache):
    try:
        tache = Tache.objects.get(id=id_tache)
        if not tache.projet.est_admin(request.user):
            raise PermissionDenied
    except Tache.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        form = TacheForm(request.POST, instance=tache)
        if form.is_valid():
            form.save()
            return redirect('projet', tache.projet.id)
    else:
        form = TacheForm(instance=tache)
    edit = True
    preconfigurations = PreconfigurationTache.objects.all()
    return render(request, 'edit_tache.html', locals())


@can_access_projet
def ajout_creneau(request, id_tache):
    try:
        tache = Tache.objects.get(id=id_tache)
        if not tache.projet.est_admin(request.user):
            raise PermissionDenied
    except Tache.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        form = CreneauForm(request.POST)
        if form.is_valid():
            creneau = form.save(commit=False)
            creneau.tache = tache
            creneau.save()
            return redirect('projet', tache.projet.id)
    else:
        form = CreneauForm()
    edit = False
    return render(request, 'edit_creneau.html', locals())


@can_access_projet
def edit_creneau(request, id_creneau):
    try:
        creneau = Creneau.objects.get(id=id_creneau)
        if not creneau.tache.projet.est_admin(request.user):
            raise PermissionDenied
    except Creneau.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        form = CreneauForm(request.POST, instance=creneau)
        if form.is_valid():
            form.save()
            return redirect('projet', creneau.tache.projet.id)
    else:
        form = CreneauForm(instance=creneau)
    edit = True
    return render(request, 'edit_creneau.html', locals())


@can_access_projet
def suppression_creneau(request):
    if request.method == 'POST':
        try:
            creneau = Creneau.objects.get(id=request.POST['id_creneau'])
            if not creneau.tache.projet.est_admin(request.user):
                raise PermissionDenied
            creneau.delete()
        except Creneau.DoesNotExist:
            return JsonResponse({
                'erreur': "Le créneau n'existe pas (id={}) !".format(
                    request.POST['id_creneau'])})

    return JsonResponse({'success': True})


@can_access_projet
def suppression_participant_projet(request):
    if request.method == 'POST':
        try:
            projet = Projet.objects.accessibles(request.user).get(
                id=request.POST['id_projet'])
            participant = ParticipantProjet.objects.get(
                id=request.POST['id_participant'])
            if not projet.est_admin(request.user):
                raise PermissionDenied
            participant.delete()
        except Creneau.DoesNotExist:
            return JsonResponse({
                'erreur': "Le créneau n'existe pas (id={}) !".format(
                    request.POST['id_creneau'])})
        except ParticipantProjet.DoesNotExist:
            return JsonResponse(
                {'erreur': "Le participant n'existe pas (id={}) !".format(
                    request.POST['id_participant'])})

    return JsonResponse({'success': True})


@can_access_projet
def suppression_participant_tache(request):
    if request.method == 'POST':
        try:
            tache = Tache.objects.get(id=request.POST['id_tache'])
            participant = ParticipantProjet.objects.get(
                id=request.POST['id_participant'])
            if not tache.projet.est_admin(request.user):
                raise PermissionDenied
            tache.membreAssigne.remove(participant)
        except Tache.DoesNotExist:
            return JsonResponse({
                'erreur': "La tâche n'existe pas (id={}) !".format(
                    request.POST['id_tache'])})
        except ParticipantProjet.DoesNotExist:
            return JsonResponse(
                {'erreur': "Le participant n'existe pas (id={}) !".format(
                    request.POST['id_participant'])})

    return JsonResponse({'success': True})


@can_access_projet
def suppression_participant_creneau(request):
    if request.method == 'POST':
        try:
            creneau = Creneau.objects.get(id=request.POST['id_creneau'])
            participant = ParticipantProjet.objects.get(
                id=request.POST['id_participant'])
            if not creneau.tache.projet.est_admin(request.user):
                raise PermissionDenied
            creneau.membreAssigne.remove(participant)
        except Creneau.DoesNotExist:
            return JsonResponse({
                'erreur': "Le créneau n'existe pas (id={}) !".format(
                    request.POST['id_creneau'])})
        except ParticipantProjet.DoesNotExist:
            return JsonResponse(
                {'erreur': "Le participant n'existe pas (id={}) !".format(
                    request.POST['id_participant'])})

    return JsonResponse({'success': True})


@can_access_projet
def terminer_tache(request):
    if request.method == 'POST':
        try:
            tache = Tache.objects.get(id=request.POST['id_tache'])
            if not tache.est_inscrit(request.user):
                raise PermissionDenied
            tache.tacheTerminee = True
            tache.save()
            if tache.notifBureauTermine:
                Bot('tache_terminee', channel=Bot.CHAN_BUREAU,
                    data=locals()).send()
        except Tache.DoesNotExist:
            return JsonResponse({
                'erreur': "La tâche n'existe pas (id={}) !".format(
                    request.POST['id_tache'])})

    return JsonResponse({'success': True})


@can_access_projet
def suppression_tache(request):
    if request.method == 'POST':
        try:
            tache = Tache.objects.get(id=request.POST['id_tache'])
            if not tache.projet.est_admin(request.user):
                raise PermissionDenied
            tache.delete()
        except Tache.DoesNotExist:
            return JsonResponse({
                'erreur': "La tâche n'existe pas (id={}) !".format(
                    request.POST['id_tache'])})

    return JsonResponse({'success': True})


@can_access_projet
def get_fichier_tache(request, fichier_tache_id: int):
    """
    Permet aux utilisateurs connectés de récupérer les objets FichierTache

    :param request:
    :param fichier_tache_id: id du FichierTache que l'on veut récupérer
    :return:
    """
    fichier_tache = get_object_or_404(FichierTache, id=fichier_tache_id)
    return media_response(fichier_tache.fichier.url)


@register(url='projets/supprimer', name='supprimer_projets',
          verbose_name="Supprimer tous les projets",
          fa_icon="fas fa-trash", btn_class="btn-danger delete")
@can_access_passation
def supprimer_projets(request):
    """ Supprime tous les projets

    :param request:
    :return:
    """
    for projet in Projet.objects.all():
        projet.delete()
    return redirect('passation')
