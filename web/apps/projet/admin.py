# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import TypeProjet, TypeParticipantProjet, Projet, ParticipantProjet, Tache, Creneau, FichierTache, PreconfigurationTache


@admin.register(TypeProjet)
class TypeProjetAdmin(admin.ModelAdmin):
    list_display = ('typeProjet',)


@admin.register(TypeParticipantProjet)
class TypeParticipantProjetAdmin(admin.ModelAdmin):
    list_display = ('typeParticipant',)


@admin.register(Projet)
class ProjetAdmin(admin.ModelAdmin):
    list_display = (
        'nom',
        'type',
        'deadlineRendu',
        'description',
        'statut',
    )
    list_filter = ('type', 'deadlineRendu')


@admin.register(ParticipantProjet)
class ParticipantProjetAdmin(admin.ModelAdmin):
    list_display = (
        'projet',
        'utilisateur',
        'admin',
        'numeroDeFicheCession',
    )
    list_filter = ('projet', 'utilisateur', 'admin')
    raw_id_fields = ('typeParticipant',)


@admin.register(Tache)
class TacheAdmin(admin.ModelAdmin):
    list_display = (
        'projet',
        'nom',
        'tacheTerminee',
        'description',
        'visible',
        'maxMembreAssignable',
    )
    list_filter = (
        'projet',
        'tacheTerminee',
        'visible',
        'ficheCessionDroits',
        'notifPourInscription',
        'notifBureauTermine',
        'notifBureauAvantDate',
        'notifMembreAvantDate',
        'notifMembreApresDate',
        'notifBureau',
        'notifMembre',
        'diffusee',
    )
    raw_id_fields = ('membreAssigne', 'emprunts')


@admin.register(Creneau)
class CreneauAdmin(admin.ModelAdmin):
    list_display = ('tache', 'debut', 'fin', 'maxMembreAssignable')
    list_filter = ('tache', 'debut', 'fin')
    raw_id_fields = ('membreAssigne',)


@admin.register(FichierTache)
class FichierTacheAdmin(admin.ModelAdmin):
    list_display = ('tache', 'fichier', 'utilisateur')
    list_filter = ('tache', 'utilisateur')


@admin.register(PreconfigurationTache)
class PreconfigurationTacheAdmin(admin.ModelAdmin):
    list_display = (
        'nom',
        'nomTache',
        'visible',
        'maxMembreAssignable',
        'ficheCessionDroits',
        'notifPourInscription',
        'notifBureauTermine',
        'notifBureauAvantDate',
        'notifMembreAvantDate',
        'notifMembreApresDate',
        'notifBureau',
        'notifMembre',
    )
    list_filter = (
        'visible',
        'ficheCessionDroits',
        'notifPourInscription',
        'notifBureauTermine',
        'notifBureauAvantDate',
        'notifMembreAvantDate',
        'notifMembreApresDate',
        'notifBureau',
        'notifMembre',
    )

