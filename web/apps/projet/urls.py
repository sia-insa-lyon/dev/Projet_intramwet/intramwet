from django.urls import path

from .views import get_fichier_tache, projet_liste, projet, ajout_projet, \
    edit_projet, statut_projet, edit_type_projet, delete_type_projet, \
    manage_types_projet, ajout_tache, edit_tache, ajout_creneau, edit_creneau, \
    ajout_participant_projet, suppression_participant_projet, \
    ajout_participant_creneau, ajout_participant_tache, suppression_creneau, \
    suppression_participant_tache, suppression_tache, terminer_tache, \
    tache_details, projet_description, diffuser_tache, \
    suppression_participant_creneau, valider_projet

urlpatterns = [
    path('', projet_liste, name='projet_liste'),
    path('<int:id_projet>/', projet, name='projet'),
    path('<int:id_projet>/valider/', valider_projet, name='valider_projet'),

    path('ajout/', ajout_projet, name='ajout_projet'),
    path('<int:id_projet>/edit/',edit_projet, name='edit_projet'),
    path('statut/', statut_projet, name='statut_projet'),

    path('type/<int:id>/modification', edit_type_projet,
         name='modification_type_projet'),
    path('type/<int:id>/suppression', delete_type_projet,
         name='suppression_type_projet'),
    path('type', manage_types_projet, name='gestion_types_projet'),

    path('projet/<int:id_projet>/tache/ajout/', ajout_tache,
         name='ajout_tache'),
    path('tache/<int:id_tache>/edit/', edit_tache, name='edit_tache'),

    path('tache/<int:id_tache>/creneau/ajout/', ajout_creneau,
         name='ajout_creneau'),
    path('creneau/<int:id_creneau>/edit/', edit_creneau,
         name='edit_creneau'),

    path('participant/ajout', ajout_participant_projet,
         name='ajout_participant_projet'),
    path('participant/suppression', suppression_participant_projet,
         name='suppression_participant_projet'),

    path('participant/creneau/ajout', ajout_participant_creneau,
         name='ajout_participant_creneau'),
    path('participant/tache/ajout', ajout_participant_tache,
         name='ajout_participant_tache'),
    path('creneau/suppression', suppression_creneau,
         name='suppression_creneau'),
    path('participant/creneau/suppression',
         suppression_participant_creneau,
         name='suppression_participant_creneau'),
    path('participant/tache/suppression', suppression_participant_tache,
         name='suppression_participant_tache'),

    path('tache/suppression', suppression_tache,
         name='suppression_tache'),
    path('tache/terminer', terminer_tache, name='terminer_tache'),

    path('tache/', tache_details, name='tache_details'),
    path('description/', projet_description, name='projet_description'),
    path('tache/fichier/<int:fichier_tache_id>', get_fichier_tache,
         name="telecharger_fichier_tache"),

    path('diffuser/', diffuser_tache, name='diffuser_tache'),
]
