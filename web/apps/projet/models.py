from datetime import datetime, timezone

from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.utils.functional import cached_property

from apps.materiel.models import EmpruntMateriel


class TypeProjetManager(models.Manager):
    def accessibles(self, utilisateur: get_user_model()):
        return self.all() if utilisateur.is_staff else [t for t in self.all()
                                                        if
                                                        utilisateur.has_perm(
                                                            t.full_permission_name)]


class TypeProjet(models.Model):
    typeProjet = models.CharField(max_length=200)
    objects = TypeProjetManager()

    @property
    def permission_name(self):
        return "voir_type_projet_{}".format(self.id)

    @cached_property
    def full_permission_name(self):
        return "projet.{}".format(self.permission_name)

    def create_permission(self):
        content_type = ContentType.objects.get_for_model(TypeProjet)
        Permission.objects.create(
            codename=self.permission_name,
            name='Peut accéder aux projets du type {}'.format(self.id),
            content_type=content_type,
        )

    def get_permission(self):
        content_type = ContentType.objects.get_for_model(TypeProjet)
        return Permission.objects.get(
            codename=self.permission_name,
            content_type=content_type,
        )

    def delete_permission(self):
        content_type = ContentType.objects.get_for_model(TypeProjet)
        Permission.objects.get(codename=self.permission_name,
                               content_type=content_type).delete()

    def __str__(self):
        return self.typeProjet


@receiver(post_save, sender=TypeProjet)
def init_permission_on_type_projet_created(sender, instance: TypeProjet,
                                           created: bool, **kwargs):
    if created:
        instance.create_permission()


@receiver(pre_delete, sender=TypeProjet)
def delete_permission_on_type_projet_deleted(sender, instance: TypeProjet,
                                             **kwargs):
    instance.delete_permission()


class TypeParticipantProjet(models.Model):
    typeParticipant = models.CharField("Type de participant", max_length=200)

    def __str__(self):
        return self.typeParticipant


class ProjetManager(models.Manager):
    def accessibles(self, utilisateur: get_user_model()):
        return self.all().filter(
            type__in=TypeProjet.objects.accessibles(utilisateur))


class Projet(models.Model):
    nom = models.CharField(max_length=200)
    type = models.ForeignKey(TypeProjet, on_delete=models.PROTECT)
    deadlineRendu = models.DateTimeField("Deadline de rendu", null=True,
                                         blank=True)
    description = models.TextField(null=True, blank=True)
    objects = ProjetManager()

    VALIDE = "VAL"
    EN_ATTENTE = "ATT"
    FINI = "FIN"
    statut = models.CharField(
        max_length=20,
        choices=[
            (VALIDE, "Validé"),
            (EN_ATTENTE, "En attente"),
            (FINI, "Fini"),
        ],
        default=EN_ATTENTE,
    )

    def est_admin(self, user):
        return ParticipantProjet.objects.filter(projet=self, utilisateur=user,
                                                admin=True).exists() \
               or user.is_staff

    def __str__(self):
        return "%s | %s" % (self.type, self.nom)


class ParticipantProjet(models.Model):
    projet = models.ForeignKey(Projet, on_delete=models.CASCADE,
                               related_name="participants")
    utilisateur = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    typeParticipant = models.ManyToManyField(TypeParticipantProjet)
    admin = models.BooleanField("Administrateur de la tâche", default=False)
    numeroDeFicheCession = models.IntegerField(
        "Numero de fiche de cession de droits", null=True, blank=True)

    def __str__(self):
        return "%s | %s" % (self.projet, self.utilisateur)


class Tache(models.Model):
    projet = models.ForeignKey(Projet, on_delete=models.CASCADE,
                               related_name='taches')
    nom = models.CharField("Nom de la tache", max_length=200)
    tacheTerminee = models.BooleanField("Tache réalisée", default=False)
    description = models.TextField(null=True, blank=True)
    visible = models.BooleanField("Visible par les membres", default=False)
    membreAssigne = models.ManyToManyField(ParticipantProjet, blank=True)
    maxMembreAssignable = models.IntegerField(
        "Nombre de membre(s) maximal assignable", default=0)

    emprunts = models.ManyToManyField(EmpruntMateriel, blank=True)

    ficheCessionDroits = models.BooleanField(
        "Faire générer une fiche de cession de droits aux membres inscrits",
        default=False)

    notifPourInscription = models.BooleanField(
        "Notifier les membres si personne n'est inscrit", default=True)
    notifBureauTermine = models.BooleanField(
        "Notifier le bureau si la tâche est terminée", default=True)

    # Si la tâche possède un créneau
    notifBureauAvantDate = models.BooleanField(
        "Notifier le bureau si personne n'est inscrit avant la date",
        default=True)
    notifMembreAvantDate = models.BooleanField(
        "Notifier le membre inscrit juste avant la date", default=True)
    notifMembreApresDate = models.BooleanField(
        "Notifier le membre inscrit après la date pour avoir confirmation de la réalisation de la tâche",
        default=True)

    # Si la tâche ne possède pas de créneau
    notifBureau = models.BooleanField("Notifier périodiquement le bureau",
                                      default=True)
    notifMembre = models.BooleanField(
        "Notifier périodiquement le membre inscrit", default=True)

    diffusee = models.BooleanField("A été diffusée massivement aux membres",
                                   default=False)

    def necessite_cession_droits(self, user):
        return not FichierTache.objects.filter(utilisateur=user,
                                               tache__projet=self.projet).exists()

    def est_inscrit(self, user):
        return self.projet.est_admin(user) \
               or Creneau.objects.filter(tache=self,
                                         membreAssigne__utilisateur=user).exists() \
               or self.membreAssigne.filter(utilisateur=user).exists()

    def est_notifiable_pour_inscription(self):
        notifiable = False
        if len(self.membreAssigne.all()) < self.maxMembreAssignable:
            notifiable = True
        for creneau in self.creneaux.all():
            if creneau.est_notifiable_pour_inscription():
                notifiable = True
        return notifiable

    def __str__(self):
        return "%s | %s " % (self.projet, self.nom)


class Creneau(models.Model):
    tache = models.ForeignKey(Tache, on_delete=models.CASCADE,
                              related_name='creneaux')
    debut = models.DateTimeField("Date de début")
    fin = models.DateTimeField("Date de fin", null=True, blank=True)

    membreAssigne = models.ManyToManyField(ParticipantProjet, blank=True)
    maxMembreAssignable = models.IntegerField(
        "Nombre de membre(s) maximal assignable", default=0)

    def est_notifiable_pour_inscription(self):
        return not self.tache.tacheTerminee \
               and self.tache.projet.statut == Projet.VALIDE \
               and self.tache.visible \
               and len(self.membreAssigne.all()) < self.maxMembreAssignable \
               and datetime.now(timezone.utc) < self.debut

    def __str__(self):
        return "%s | %s" % (self.tache, self.debut)


# TODO gérer réinitialisation en fin d'année
class FichierTache(models.Model):
    tache = models.ForeignKey(Tache, on_delete=models.PROTECT,
                              related_name='fichiers')
    fichier = models.FileField(upload_to='fiches_cession_droits/')

    utilisateur = models.ForeignKey(get_user_model(), on_delete=models.PROTECT,
                                    null=True, blank=True)

    @cached_property
    def filename(self):
        return "CD_{}_{}.pdf".format(self.tache.projet.nom,
                                     self.utilisateur.nomComplet).replace(' ',
                                                                          '_').replace(
            "'",
            '_').replace(
            'é', 'e').replace('è', 'e').replace('ê', 'e') \
            .replace('à', 'a').replace('ù', 'u').replace('"', '_').replace('(',
                                                                           '_').replace(
            ')', '_') \
            .replace('[', '_').replace(']', '_').replace('{', '_').replace('}',
                                                                           '_').replace(
            '\\', '')

    def __str__(self):
        return self.filename


class PreconfigurationTache(models.Model):
    nom = models.CharField("Nom de la préconfiguration", max_length=200)
    nomTache = models.CharField("Nom de la tache", max_length=200, null=True,
                                blank=True)
    visible = models.BooleanField("Visible par les membres", default=False)
    maxMembreAssignable = models.IntegerField(
        "Nombre de membre(s) maximal assignable", default=0)
    ficheCessionDroits = models.BooleanField(
        "Faire générer une fiche de cession de droits aux membres inscrits",
        default=False)

    notifPourInscription = models.BooleanField(
        "Notifier les membres si personne n'est inscrit", default=True)
    notifBureauTermine = models.BooleanField(
        "Notifier le bureau si la tâche est terminée", default=True)

    # Si la tâche possède un créneau
    notifBureauAvantDate = models.BooleanField(
        "Notifier le bureau si personne n'est inscrit avant la date",
        default=True)
    notifMembreAvantDate = models.BooleanField(
        "Notifier le membre inscrit juste avant la date", default=True)
    notifMembreApresDate = models.BooleanField(
        "Notifier le membre inscrit après la date pour avoir confirmation de la réalisation de la tâche",
        default=True)

    # Si la tâche ne possède pas de créneau
    notifBureau = models.BooleanField("Notifier périodiquement le bureau",
                                      default=True)
    notifMembre = models.BooleanField(
        "Notifier périodiquement le membre inscrit", default=True)

    def __str__(self):
        return "%s" % self.nom
