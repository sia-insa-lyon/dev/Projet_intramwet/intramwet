from datetime import datetime, timezone, timedelta

import django_rq
from django.apps import AppConfig


class ProjetConfig(AppConfig):
    name = 'apps.projet'

    def ready(self):
        from .tasks import daily_task, weekly_task, notif_reguliere, \
            notif_quotidienne, notif_pour_inscription
        scheduler = django_rq.get_scheduler()

        # Delete any existing jobs in the scheduler when the app starts up
        jobs = scheduler.get_jobs()
        for job in filter(lambda x: x.func in [daily_task, weekly_task,
                                               notif_pour_inscription,
                                               notif_quotidienne,
                                               notif_reguliere], jobs):
            scheduler.cancel(job)

        # Schedule tout les jours à 8h
        scheduler.schedule(
            scheduled_time=(datetime.now(timezone.utc) + timedelta(
                days=1)).replace(hour=6, minute=0),
            func=daily_task,
            interval=60 * 60 * 24
        )

        # Schedule tout les lundis à 8h
        weekday = datetime.now(timezone.utc).weekday()
        scheduler.schedule(
            scheduled_time=(datetime.now(timezone.utc) + timedelta(
                days=7 - weekday)).replace(hour=6, minute=0),
            func=weekly_task,
            interval=60 * 60 * 24 * 7
        )
