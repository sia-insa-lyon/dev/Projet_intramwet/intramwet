from apps.conf.utils import get_prefix
from apps.projet.models import Projet


def notify_new_projet(request, projet: Projet):
    from ..conf.bot import Email
    from ..utilisateur.models import MembreCA

    prefix = get_prefix(request)
    for email in MembreCA.objects.emails_to_notify_for_projet():
        Email('emails/notification_projet_bureau.html', email, locals()).send()