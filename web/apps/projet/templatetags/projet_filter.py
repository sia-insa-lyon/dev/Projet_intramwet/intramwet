from datetime import datetime, timezone

from django import template

register = template.Library()


@register.filter
def ajouter_fin_row_tache(taches_length):
    return taches_length % 3 == 2


@register.filter
def duree(debut, fin):
    hour = int((fin - debut).seconds / 3600)
    min = int(((fin - debut).seconds - hour * 3600) / 60)
    if min == 0:
        return "%sh" % hour
    elif hour == 0:
        return "%smin" % min
    else:
        return "%sh %smin" % (hour, min)


@register.filter
def admin(user, tache):
    return tache.projet.est_admin(user)


@register.filter
def necessite_cession_droits(user, tache):
    return tache.necessite_cession_droits(user)


@register.simple_tag
def annee():
    date = datetime.now(timezone.utc)
    year = int(date.strftime('%y'))
    year1 = year - 1 if date.month < 9 else year
    year2 = year if date.month < 9 else year + 1
    return "20{}-20{}".format(year1, year2)


@register.filter
def replacelinebreaks(text):
    return text.replace('\n', '<br/>')
