import random
from datetime import timedelta, datetime, timezone

from django.contrib.auth import get_user_model
from django_rq import job, get_scheduler

from apps.conf.bot import Bot, Email
from .models import Tache, Projet, ParticipantProjet, Creneau


@job
def daily_task():
    taches = Tache.objects.exclude(projet__statut=Projet.FINI).exclude(
        tacheTerminee=True)
    scheduler = get_scheduler()

    # Répartition entre 8h et 00h
    for tache in taches:
        scheduler.enqueue_in(timedelta(minutes=random.randint(1, 60 * 16)),
                             notif_quotidienne, tache.id)


@job
def weekly_task():
    taches = Tache.objects.exclude(projet__statut=Projet.FINI).exclude(
        tacheTerminee=True)
    projet_notif_inscription = Projet.objects.filter(
        tache__notifPourInscription=True, tache__tacheTerminee=False) \
        .exclude(statut=Projet.FINI).distinct()
    scheduler = get_scheduler()

    # Répartition sur toute la semaine, entre 8h et 00h
    for tache in taches:
        scheduler.enqueue_in(timedelta(days=random.randint(0, 6),
                                       minutes=random.randint(1, 60 * 16)),
                             notif_reguliere,
                             tache.id)
    for projet in projet_notif_inscription:
        scheduler.enqueue_in(timedelta(days=random.randint(0, 6),
                                       minutes=random.randint(1, 60 * 16)),
                             notif_pour_inscription, projet.id)


@job
def notif_pour_inscription(id_projet):
    """
    Envoie régulièrement des notifications si personne n'est inscrit à certaines tâches d'un projet
    """
    projet = Projet.objects.get(id=id_projet)
    notifier = False
    tache = None
    for t in projet.tache_set.all():
        if t.notifPourInscription and t.est_notifiable_pour_inscription():
            notifier = True
            tache = t
    if notifier:
        Bot('notif_pour_inscription', Bot.CHAN_MEMBRE,
            data={'tache': tache}).send()


@job
def notif_quotidienne(id_tache):
    """
    Envoie une notification au bureau si une tâche arrive sans personne d'inscrit.
    Si quelqu'un est inscrit et que la tâche arrive bientôt, on envoi un mail à l'inscrit.
    Si la tâche est passé, envoi un mail de confirmation aux membres inscrits.
    """
    tache = Tache.objects.get(id=id_tache)
    if not tache.tacheTerminee:
        est_inscrit = False
        est_bientot = False
        est_passe = False
        demain = datetime.now(timezone.utc) + timedelta(days=1)
        hier = datetime.now(timezone.utc) - timedelta(days=1)
        if tache.membreAssigne.all().exists():
            est_inscrit = True

        if tache.creneaux.all():
            creneaux = tache.creneaux.all()
        else:
            creneaux = [Creneau(tache=tache, debut=tache.projet.deadlineRendu)]

        for creneau in creneaux:
            if creneau.membreAssigne.all().exists():
                est_inscrit = True

                if demain.day == creneau.debut.day and demain.month == creneau.debut.month and tache.notifMembreAvantDate:
                    admins = ParticipantProjet.objects.filter(
                        projet=tache.projet, admin=True)
                    Email('emails/rappel_tache_bientot.html',
                          data=locals()).send_mass(
                        creneau.membreAssigne.all().values_list(
                            'utilisateur__email', flat=True))

                if hier.day == creneau.fin.day and hier.month == creneau.fin.month and tache.notifMembreApresDate:
                    Email('emails/rappel_tache_finie.html',
                          data=locals()).send_mass(
                        creneau.membreAssigne.all().values_list(
                            'utilisateur__email', flat=True))

            if demain.day == creneau.debut.day and demain.month == creneau.debut.month:
                est_bientot = True

        if not est_inscrit and est_bientot and tache.notifBureauAvantDate:
            Bot('notif_bureau_avant_date', channel=Bot.CHAN_BUREAU,
                data=locals()).send()


@job
def notif_reguliere(id_tache):
    """
    Notifie régulièrement le bureau et les membres inscrits.
    """
    tache = Tache.objects.get(id=id_tache)
    if not tache.tacheTerminee:
        if tache.notifBureau:
            Bot('notif_bureau_reguliere', channel=Bot.CHAN_BUREAU,
                data=locals()).send()
        if tache.notifMembre:
            admins = ParticipantProjet.objects.filter(projet=tache.projet,
                                                      admin=True)
            users = get_user_model().objects.filter(
                participantprojet__creneau__tache=tache) \
                .union(get_user_model().objects.filter(
                participantprojet__tache=tache)).distinct()
            Email('emails/rappel_regulier.html', data=locals()).send_mass(
                users.values_list('email', flat=True))

        scheduler = get_scheduler()
        scheduler.enqueue_in(timedelta(weeks=1), notif_reguliere, tache.id)
