import ntpath
import os
import zipfile
from datetime import datetime
from os.path import exists
from typing import List
from urllib.parse import urlparse

from django.http import HttpResponse


def path_leaf(path: str) -> str:
    """
    Extracts the filename from a given path
    :param path: the path of the file
    :return: the filename
    """
    head, tail = ntpath.split(path)
    return tail or ntpath.basename(head)


def add_to_zipfile(zip_file: zipfile.ZipFile, file_list: List[str],
                   folder_name: str = None) -> zipfile.ZipFile:
    """
    Adds all files from the list to the ZipFile (potentially in a specified folder)

    :param zip_file: the ZipFile object to which the files will be added
    :param file_list: the list of file paths to add
    :param folder_name: name of the folder which will contain the files inside the archive (no folder if left empty)
    :return: the zipfile object itself
    """

    for file in file_list:
        if exists(file):
            file_name = path_leaf(file)
            if folder_name:
                zip_file.write(filename=file, arcname=f'{folder_name}/{file_name}')
            else:
                zip_file.write(filename=file, arcname=file_name)
    return zip_file


def get_zipped_response(file_list: List[str], archive_name: str = None,
                        folder_name: str = None) -> HttpResponse:
    """
    Downloads all files in zip format
    :param file_list: list of file paths to compress
    :param archive_name: the name of the archive (only useful when creating the response)
    :param folder_name: name of the folder which will contain the files inside the archive (no folder if left empty)
    :return: an HTTP Response with a zip of all files in the list
    """

    response = HttpResponse(content_type='application/zip')
    response['Content-Disposition'] = f'attachment; filename={archive_name}'
    with zipfile.ZipFile(response, 'w') as z:
        add_to_zipfile(zip_file=z, file_list=file_list,
                       folder_name=folder_name)

    return response


def get_schoolyear() -> int:
    """
    Get the current schoolyear
    :return: the current schoolyear in double-digit format (1819 for 2018-2019)
    """
    now = datetime.now()
    if now.month > 6:
        annee1 = int(now.strftime("%y"))
        annee2 = annee1 + 1
    else:
        annee2 = int(now.strftime("%y"))
        annee1 = annee2 - 1
    return annee1 * 100 + annee2


def format_file_title(title: str) -> str:
    return title.replace("'", '_').replace('é', 'e').replace('è', 'e').replace(
        'ê', 'e').replace('à', 'a') \
        .replace('ù', 'u').replace('"', '_').replace('(', '_').replace(')',
                                                                       '_').replace(
        '[', '_') \
        .replace(']', '_').replace('{', '_').replace('}', '_').replace('\\',
                                                                       '') if title else ''


def media_response(path: str) -> HttpResponse:
    """
    Permet de construire une réponse HTTP avec la fonctionnalité X-Accel-Redirect de NGINX. Cette fonction est utilisée
    par toutes les autres vues.

    Cette méthode permet de servir les fichiers media avec NGINX (avec de meilleures performances, caching, etc) tout
    en les protégeant. Pour plus d'info, voir ici: https://wellfire.co/learn/nginx-django-x-accel-redirects/.

    Cette fonction détecte automatiquement le type de fichier grâce à l'extension. Si vous voulez servir de nouveaux
    types de fichiers, il faut rajouter l'extension et le Content-Type correspondant dans le dictionnaire content_types.

    :param path: URL du fichier demandé par l'utilisateur
    :return: réponse HTTP utilisant X-Accel-Redirect
    """
    _, extension = os.path.splitext(path)
    content_types = {
        '.pdf': 'application/pdf',
        '.jpg': 'image/jpeg',
        '.jpeg': 'image/jpeg',
        '.png': 'image/png'
    }
    response = HttpResponse(content_type=content_types.get(extension, ""))
    response["Content-Disposition"] = "attachment; filename={0}".format(
        os.path.basename(path))
    response['X-Accel-Redirect'] = "/private_file" + path
    return response



def get_prefix(request):
    request_url = urlparse(request.build_absolute_uri())
    return f"{request_url.scheme}://{request_url.netloc}"