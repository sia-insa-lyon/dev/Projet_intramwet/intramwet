from django.core.validators import MinValueValidator, MaxValueValidator, \
    FileExtensionValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.utilisateur.models import Utilisateur


class AssociationLegalProperties(models.Model):
    name = models.CharField(
        "Nom de l'association",
        max_length=200
    )
    short_name = models.CharField(
        "Nom court de l'association",
        max_length=200,
        null=True, blank=True
    )
    legal_status = models.CharField(
        "Statut juridique de l'assocaition",
        max_length=200
    )
    siren = models.BigIntegerField(
        "Numéro SIREN de l'association",
        null=True, blank=True,
        validators=[
            MinValueValidator(000000000),
            MaxValueValidator(999999999)
        ]
    )
    siret = models.BigIntegerField(
        "Numéro SIRET de l'association",
        null=True, blank=True,
        validators=[
            MinValueValidator(00000000000000),
            MaxValueValidator(99999999999999)
        ]
    )
    address = models.TextField(
        "Lieu-dit de l'adresse juridique de l'association"
    )
    address_city = models.CharField(
        "Ville de l'adresse juridique de l'association",
        max_length=50
    )
    address_postal_code = models.IntegerField(
        "Code postal de l'adresse juridique de l'association"
    )
    address_postal_code_cedex = models.BooleanField(
        "Si le code postal de l'adresse juridique est un code CEDEX",
        default=False
    )
    address_country = models.CharField(
        "Pays de l'adresse juridique de l'association",
        max_length=50
    )
    association_articles = models.FileField(
        "Statuts de l'association en PDF",
        validators=[
            FileExtensionValidator(allowed_extensions=['pdf'])
        ],
        upload_to='public/documents/%Y/%m/%d/',
        null=True, blank=True
    )
    association_bylaw = models.FileField(
        "Règlement intérieur de l'association en PDF",
        validators=[
            FileExtensionValidator(allowed_extensions=['pdf'])
        ],
        upload_to='public/documents/%Y/%m/%d/',
        null=True, blank=True
    )
    short_description = models.CharField(
        "Description courte de l'association",
        max_length=200
    )
    data_retention = models.PositiveSmallIntegerField(
        "durée de conservation des données en mois",
        default=36
    )


class AssociationSocialProperties(models.Model):
    contact_email = models.EmailField(
        "Adresse mail de contact"
    )
    support_email = models.EmailField(
        "Adresse mail de support"
    )
    office_address = models.TextField(
        "Addresse du local de l'association"
    )
    office_address_latitude = models.DecimalField(
        "Latitude de l'adresse du local (pour la page d'accueil)",
        max_digits=18, decimal_places=15,
        null=True, blank=True
    )
    office_address_longitude = models.DecimalField(
        "Longitude de l'adresse du local (pour la page d'accueil)",
        max_digits=18, decimal_places=15,
        null=True, blank=True
    )
    permanence = models.TextField(
        "Horaires de permanence de l'association",
        null=True, blank=True
    )
    meetup = models.TextField(
        "Horaires de réunion de l'association",
        null=True, blank=True
    )
    youtube_channel = models.URLField(
        "Chaine YouTube de l'association",
        null=True, blank=True
    )
    twitter_account = models.URLField(
        "Compte Twitter de l'association",
        null=True, blank=True
    )
    facebook_account = models.URLField(
        "Page Facebook de l'association",
        null=True, blank=True
    )
    instagram_account = models.URLField(
        "Compte Instagram de l'association",
        null=True, blank=True
    )
    website = models.URLField(
        "Site vitrine de l'association",
        null=True, blank=True
    )


class AssociationGraphics(models.Model):
    logo_png = models.ImageField(
        "Logo en .png (à afficher sur fond blanc)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/logos/%Y/%m/%d/'
    )
    logo_jpg = models.ImageField(
        "Logo en .jpg (sur fond blanc, affiché sur les documents PDF)",
        validators=[
            FileExtensionValidator(allowed_extensions=['jpg'])
        ],
        upload_to='public/logos/%Y/%m/%d/'
    )
    logo_negatif_png = models.ImageField(
        "Logo négatif en .png (à afficher sur fond sombre)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/logos/%Y/%m/%d/'
    )
    og_image_jpg = models.ImageField(
        "Image utilisée par le protocole OpenGraph pour représenter le site dans la recherche (format 464x886px)",
        validators=[
            FileExtensionValidator(allowed_extensions=['jpg'])
        ],
        upload_to='public/og/%Y/%m/%d/'
    )
    favicon_16_png = models.ImageField(
        "Favicon au format 16x16px (en .png)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )
    favicon_32_png = models.ImageField(
        "Favicon au format 32x32px (en .png)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )
    favicon_150_png = models.ImageField(
        "Favicon au format 150x150px (en .png)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )
    favicon_180_png = models.ImageField(
        "Favicon au format 180x180px (en .png)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )
    favicon_192_png = models.ImageField(
        "Favicon au format 192x192px (en .png)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )
    favicon_512_png = models.ImageField(
        "Favicon au format 512x512px (en .png)",
        validators=[
            FileExtensionValidator(allowed_extensions=['png'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )
    favicon_pinned_svg = models.FileField(
        "Favicon au format .svg (pour les fenêtres épinglées)",
        validators=[
            FileExtensionValidator(allowed_extensions=['svg'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )
    favicon_ico = models.ImageField(
        "Favicon au format .ico",
        validators=[
            FileExtensionValidator(allowed_extensions=['ico'])
        ],
        upload_to='public/favicon/%Y/%m/%d/'
    )


class WebsiteProperties(models.Model):
    intramwet_name = models.CharField(
        "Nom du site web",
        max_length=200
    )
    intramwet_homepage_activated = models.BooleanField(
        "Si la page d'accueil est activée ou on redirige directement vers la page de connexion",
        default=False
    )
    intramwet_equipement_icon = models.CharField(
        "Icône utilisée pour le module de gestion de matériel",
        max_length=50
    )
    intramwet_project_icon = models.CharField(
        "Icône utilisée pour le module de gestion de projets",
        max_length=50
    )
    intramwet_url = models.URLField(
        "URL de déploiement du site"
    )
    intramwet_hosting_company_name = models.CharField(
        "Nom de l'hébergeur du site",
        max_length=200
    )
    intramwet_hosting_company_address = models.TextField(
        "Coordonnées de l'hébergeur du site"
    )
    intramwet_hosting_location = models.CharField(
        "Pays d'hébergement du site",
        max_length=200
    )
    intramwet_map_center_latitude = models.DecimalField(
        "Latitude du centre de la carte (pour la page d'accueil)",
        max_digits=10, decimal_places=7,
        null=True, blank=True
    )
    intramwet_map_center_longitude = models.DecimalField(
        "Longitude du centre de la carte (pour la page d'accueil)",
        max_digits=10, decimal_places=7,
        null=True, blank=True
    )
    intramwet_map_zoom_level = models.IntegerField(
        "Niveau de zoom de la carte (pour la page d'accueil)",
        null=True, blank=True
    )


class WebsiteManifestsManager(models.Manager):
    def generate(self):
        # Not generating if all properties have not been set
        if not AssociationGraphics.objects.all().exists() or not AssociationLegalProperties.objects.all().exists() or not WebsiteProperties.objects.all().exists():
            return self.none()

        from django.template.loader import render_to_string
        from django.forms import model_to_dict
        from django.core.files.base import ContentFile

        graphics = model_to_dict(AssociationGraphics.objects.last())
        props = model_to_dict(AssociationLegalProperties.objects.last())
        if not props['short_name']:
            props['short_name'] = props['name']
        config = model_to_dict(WebsiteProperties.objects.last())
        context = {key.upper(): value for key, value in {**graphics, **props, **config}.items()}

        webmanifest_content = render_to_string('site.webmanifest', context=context)
        browserconfig_content = render_to_string('browserconfig.xml', context=context)

        return self.create(
            webmanifest=ContentFile(webmanifest_content, name='site.webmanifest'),
            browserconfig=ContentFile(browserconfig_content, name='browserconfig.xml')
        )


class WebsiteManifests(models.Model):
    webmanifest = models.FileField(
        "Web app manifest",
        validators=[
            FileExtensionValidator(allowed_extensions=['webmanifest'])
        ],
        upload_to='public/manifests/%Y/%m/%d/'
    )
    browserconfig = models.FileField(
        "Browser configuration file",
        validators=[
            FileExtensionValidator(allowed_extensions=['xml'])
        ],
        upload_to='public/manifests/%Y/%m/%d/'
    )
    objects = WebsiteManifestsManager()


@receiver(post_save, sender=WebsiteProperties)
@receiver(post_save, sender=AssociationGraphics)
@receiver(post_save, sender=AssociationLegalProperties)
def update_manifests(sender, instance, created, **kwargs):
    WebsiteManifests.objects.generate()


@receiver(post_save, sender=AssociationLegalProperties)
@receiver(post_save, sender=AssociationSocialProperties)
def reset_cgu(sender, instance, created, **kwargs):
    Utilisateur.objects.reset_has_read_cgu()