from django.shortcuts import render
from django.template.loader import render_to_string
from mistune import markdown


def get_cgu_text() -> str:
    """ Permet de récupérer le text actuel des CGU"""
    from .context_processors import intramwet_settings
    raw_markdown = render_to_string('cgu.md', context=intramwet_settings())
    return markdown(raw_markdown, hard_wrap=True)


def cgu(request):
    """ Vue avec les CGU du site

    :param request:
    :return:
    """
    return render(request, 'cgu.html', {'cgu': get_cgu_text()})
