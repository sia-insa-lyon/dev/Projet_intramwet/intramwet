# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import AssociationLegalProperties, AssociationSocialProperties, AssociationGraphics, WebsiteProperties, WebsiteManifests


@admin.register(AssociationLegalProperties)
class AssociationLegalPropertiesAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'name',
        'short_name',
        'legal_status',
        'siren',
        'siret',
        'address',
        'address_city',
        'address_postal_code',
        'address_postal_code_cedex',
        'address_country',
        'association_articles',
        'association_bylaw',
        'short_description',
    )


@admin.register(AssociationSocialProperties)
class AssociationSocialPropertiesAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'contact_email',
        'support_email',
        'office_address',
        'office_address_latitude',
        'office_address_longitude',
        'permanence',
        'meetup',
        'youtube_channel',
        'twitter_account',
        'facebook_account',
        'instagram_account',
        'website',
    )


@admin.register(AssociationGraphics)
class AssociationGraphicsAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'logo_png',
        'logo_jpg',
        'logo_negatif_png',
        'og_image_jpg',
        'favicon_16_png',
        'favicon_32_png',
        'favicon_150_png',
        'favicon_180_png',
        'favicon_192_png',
        'favicon_512_png',
        'favicon_pinned_svg',
        'favicon_ico',
    )


@admin.register(WebsiteProperties)
class WebsitePropertiesAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'intramwet_name',
        'intramwet_homepage_activated',
        'intramwet_equipement_icon',
        'intramwet_project_icon',
        'intramwet_url',
        'intramwet_hosting_company_name',
        'intramwet_hosting_company_address',
        'intramwet_hosting_location',
        'intramwet_map_center_latitude',
        'intramwet_map_center_longitude',
        'intramwet_map_zoom_level',
    )


@admin.register(WebsiteManifests)
class WebsiteManifestsAdmin(admin.ModelAdmin):
    list_display = ('id', 'webmanifest', 'browserconfig')
