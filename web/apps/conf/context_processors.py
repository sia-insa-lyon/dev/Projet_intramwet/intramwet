from urllib.parse import urlparse

from django.conf import settings
from django.forms import model_to_dict
from apps.conf.models import WebsiteManifestsManager
from intramwet.core.init.conf import ConfInitializer


def export_vars(request=None):
    """Makes variables accessible in Django templates as {{VARIABLE_NAME}}"""
    request_url = urlparse(request.build_absolute_uri())
    return {
        'PRODUCTION': settings.PRODUCTION,
        'SENTRY_DSN': settings.SENTRY_DSN,
        'SENTRY_ENVIRONMENT': settings.SENTRY_ENVIRONMENT,
        'SENTRY_USER_INFO': settings.SENTRY_USER_INFO,
        'PREFIX': f"{request_url.scheme}://{request_url.netloc}"
    }


def intramwet_settings(request=None):
    """Makes all properties available in Django templates as {{VARIABLE_NAME}}"""
    from .models import WebsiteProperties, AssociationSocialProperties, AssociationGraphics, AssociationLegalProperties, WebsiteManifests

    if not AssociationLegalProperties.objects.all().exists():
        ConfInitializer.init_association_legal_props(ConfInitializer)
    association_legal_props = model_to_dict(AssociationLegalProperties.objects.last())
    if not AssociationSocialProperties.objects.all().exists():
        ConfInitializer.init_association_social_props(ConfInitializer)
    association_social_props = model_to_dict(AssociationSocialProperties.objects.last())
    if not AssociationGraphics.objects.all().exists():
        ConfInitializer.init_association_graphics(ConfInitializer)
    association_graphics = model_to_dict(AssociationGraphics.objects.last())
    if not WebsiteProperties.objects.all().exists():
        ConfInitializer.init_website_props(ConfInitializer)
    website_props = model_to_dict(WebsiteProperties.objects.last())
    if not WebsiteManifests.objects.all().exists():
        WebsiteManifestsManager.generate(WebsiteManifests.objects)
    website_manifests = model_to_dict(WebsiteManifests.objects.last())

    props = {**association_legal_props, **association_social_props, **association_graphics, **website_props, **website_manifests}
    if not props['short_name']:
        props['short_name'] = props['name']
    return {key.upper(): value for key, value in props.items()}
