from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied


def can_access_passation(function):
    def wrap(request, *args, **kwargs):
        func = staff_member_required(login_required(function))
        if request.user.has_perm('utilisateur.acceder_passation'):
            return func(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def register(url: str, name: str = None, verbose_name: str = None, fa_icon: str = None, btn_class: str = None, confirm: bool = True):
    """
    Register the given view to the passation interface:

    @register(url='membres/desactiver')
    def desactiver_membres(request) -> HttpResponse:
        pass
    """

    def _passation_action_wrapper(view):
        from .passation_action import PassationAction, passation_action_handler
        action = PassationAction(handler_view=view, url=url, name=name, verbose_name=verbose_name, fa_icon=fa_icon, btn_class=btn_class, confirm=confirm)
        passation_action_handler.register(action)
        return view
    return _passation_action_wrapper
