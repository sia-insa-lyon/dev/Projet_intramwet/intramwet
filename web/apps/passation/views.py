
from datetime import datetime

from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render

from apps.utilisateur.models import MembreCA, MandatCA
from .decorators import can_access_passation
from .forms import MembreCAFormset


@can_access_passation
def passation(request, modifier: bool = False) -> HttpResponse:
    """ Interface de passation

    Permet de changer les membres du CA, et de réinitialiser la BD pour l'année suivante
    :param request:
    :return:
    """
    template_name = 'passation.html'
    from .passation_action import passation_action_handler
    passation_actions = passation_action_handler.actions
    membresCA = MembreCA.objects.filter(
        Q(mandat__fin__isnull=True) | Q(mandat__fin__gt=datetime.today()))
    if request.method == 'POST':
        formset = MembreCAFormset(request.POST)
        if formset.is_valid():
            if modifier:
                if MandatCA.objects.exists():
                    new_ca = MandatCA.objects.latest('id')
                else:
                    new_ca = MandatCA.objects.create()
            else:
                if MandatCA.objects.exists():
                    old_ca = MandatCA.objects.latest('id')
                    old_ca.fin = datetime.today().date()
                    old_ca.save()
                new_ca = MandatCA.objects.create()
            for form in formset:
                membreCA = form.save(commit=False)
                if not modifier:
                    membreCA = MembreCA(statut=membreCA.statut,
                                        membre=membreCA.membre)
                membreCA.mandat = new_ca
                membreCA.save()
    else:
        formset = MembreCAFormset(queryset=membresCA)
    return render(request, template_name, locals())
