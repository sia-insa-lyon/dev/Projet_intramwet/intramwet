function send_form(url) {
    const form = $('form');
    if (form[0].checkValidity()) {
        form.attr("action", url);
        form.submit();
    } else form[0].reportValidity();
}

$(function () {
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]'
    });
});