from .forms import AcceptationCGUForm


def cgu_form(request=None):
    return {
        'cgu_form': AcceptationCGUForm()
    }
