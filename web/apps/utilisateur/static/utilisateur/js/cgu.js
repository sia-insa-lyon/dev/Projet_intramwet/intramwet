function acceptCGU() {
    let form = $("#formCGU");
    if (form[0].checkValidity()) {
        form.ajaxSubmit({
            url: form.action,
            type: 'POST',
            success: function (response, textStatus, xhr) {
                if (xhr.status === 200) {
                    $('#myModal').modal('hide');
                }
            }
        });
    } else form[0].reportValidity();
}