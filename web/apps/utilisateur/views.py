from datetime import datetime, timezone

import django_excel as excel
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth import authenticate, update_session_auth_hash, logout, \
    get_user_model
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.models import Group
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import redirect, render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse

from apps.treso.views import save_pdf
from apps.conf.bot import Email, Bot
from apps.conf.utils import media_response
from .decorators import can_access_membres, can_manage_membres
from .forms import InscriptionForm, ModificationForm, \
    AdminModificationForm, UsernameForgottenForm, \
    PasswordResetForm, AcceptationCGUForm
from .models import Utilisateur, FicheInscription, MandatCA, MembreCA, AcceptationCGU
from apps.treso.models.clients import Client as Organisation
from apps.passation.decorators import can_access_passation, register

##########################################################################
#                                                                        #
# Interface d'inscription, connexion et déconnexion                      #
#                                                                        #
##########################################################################


def inscription(request) -> HttpResponse:
    """ Interface d'inscription

    Utilise un formulaire asynchrone avec jQuery.
    :param request:
    :return: JsonResponse avec un code de succès et les erreurs éventuelles
    """

    if request.method == 'POST':
        print(request.POST)
        form = InscriptionForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            group = form.cleaned_data.get('groups')
            user = authenticate(username=username, password=raw_password)
            user.groups.add(group)
            user.save()

            has_accepted_cgu = form.cleaned_data.get('has_accepted_cgu')
            has_accepted_rgpd = form.cleaned_data.get('has_accepted_rgpd')
            has_accepted_cookies = form.cleaned_data.get('has_accepted_cookies')
            if has_accepted_cgu and has_accepted_rgpd and has_accepted_cookies:
                acceptation_cgu = AcceptationCGU.objects.create(
                    has_accepted_cookies=has_accepted_cookies,
                    has_accepted_cgu=has_accepted_cgu,
                    has_accepted_rgpd=has_accepted_rgpd,
                    utilisateur=user
                )
                user.has_read_cgu = True
                user.save()
                acceptation_cgu.generate_fiche()

            if user.groups.all:
                if user.groups.all()[0].description.demandePrecisions:
                    precisions = request.POST['precisions']
            Email('emails/inscription.html', user.email, locals()).send()
            Bot('inscription', channel=Bot.CHAN_BUREAU, data=locals()).send()
            return JsonResponse({
                "valid": True
            })

        return JsonResponse({
            "valid": False,
            "errors": render_to_string('registration/signup_errors.html',
                                       {'form': form})
        })

    form = InscriptionForm()
    return render(request, 'registration/signup.html',
                  {'form': form, 'next': reverse('accueil', kwargs={
                      'modal_inscription': True})})


def nom_oublie(request) -> HttpResponse:
    """ Interface de réinitialisation du nom d'utilisateur

    Renvoie un mail contenant tous les noms d'utilisateur associés à l'adresse mail spécifiée.
    :param request:
    :return:
    """
    if request.method == "POST":
        form = UsernameForgottenForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get("email")
            users = Utilisateur.objects.filter(email=email)
            Email('emails/username_forgotten.html', email, locals()).send()
            return redirect('accueil', modal_nom_utilisateur=True)
    form = UsernameForgottenForm()
    return render(request, 'registration/username_forgotten.html', locals())


class PasswordResetView(auth_views.PasswordResetView):
    """
        Surcharge de la vue de reset de mdp pour pouvoir envoyer un mail avec le moteur de mail interne
    """
    form_class = PasswordResetForm


def reset_complete(request) -> HttpResponse:
    """ Vue de confirmation de réinitialisation de mot de passe

    :param request:
    :return:
    """
    return redirect('connexion')


@login_required
def deconnexion(request) -> HttpResponse:
    """ Vue de déconnexion

    Déconnecte l'utilisateur et le renvoie à la pagge d'accueil
    :param request:
    :return:
    """
    logout(request)
    return redirect('accueil')


##########################################################################
#                                                                        #
# Profil utilisateur                                                     #
#                                                                        #
##########################################################################

@login_required
def user_view(request) -> HttpResponse:
    """ Profil utilisateur

    :param request:
    :return: le profil de l'utilisateur connecté
    """
    if request.method == 'GET':
        password_form = PasswordChangeForm(user=request.user)
        info_form = ModificationForm(instance=request.user)
        return render(request, 'profil.html', {
            'user': request.user,
            'password_form': password_form,
            'info_form': info_form
        })

    return redirect('accueil')


@login_required
def password_change(request) -> HttpResponse:
    """ Vue qui gère la modification du mot de passe

    :param request:
    :return: le profil utilisateur, une fois le mot de passe changé
    """
    if request.method == 'POST':
        password_form = PasswordChangeForm(data=request.POST,
                                           user=request.user)
        info_form = ModificationForm(instance=request.user)
        if password_form.is_valid():
            password_form.save()
            update_session_auth_hash(request, password_form.user)
            return redirect('utilisateur')

        return render(request, 'profil.html', {
            'user': request.user,
            'password_form': password_form,
            'info_form': info_form
        })

    return redirect('utilisateur')


@login_required
def user_info_change(request) -> HttpResponse:
    """ Vue qui gère la modification des informations de l'utilisateur

    :param request:
    :return: le profil utilisateur, une fois ses informations modifiées
    """
    if request.method == 'POST':
        info_form = ModificationForm(request.POST, request.FILES,
                                     instance=request.user)
        password_form = PasswordChangeForm(user=request.user)
        if info_form.is_valid():
            info_form.save()
            return redirect('utilisateur')

        return render(request, 'profil.html', {
            'user': request.user,
            'password_form': password_form,
            'info_form': info_form
        })

    return redirect('utilisateur')


@can_access_membres
def all_users(request) -> HttpResponse:
    """ Interface avec tous les utilisateurs du site

    Sont exclus les utilisateurs inactifs (dans le sens de Django), non-validés et le compte superuser (medialamouette)
    :param request:
    :return:
    """
    users = Utilisateur.objects.all().exclude(
        Q(is_active=False) | Q(actif=False) | Q(valide=False)
    ).order_by('groups', '-is_staff','last_name')
    return render(request, 'all_users.html', {'users': users})


@can_access_membres
def administrators(request) -> HttpResponse:
    """ Interface avec tous les bureaux
    :param request:
    :return:
    """
    mandats = MandatCA.objects.order_by('-id')
    return render(request, 'bureaux.html', locals())


@can_access_membres
def public_profile(request, uuid: int):
    user = get_object_or_404(Utilisateur, id=uuid)
    return render(request, 'public_profile.html', locals())


@can_access_membres
def get_user_profile_picture(request, user_id: int):
    """
    Permet aux utilisateurs connectés de récupérer les photos de profil des autres utilisateurs (et, entre autre, leur
    propre photo de profil)

    :param request:
    :param user_id: identifiant de l'utilisateur dont on veut récupérer l'image
    :return:
    """
    user = get_object_or_404(Utilisateur, id=user_id)
    return media_response(path=user.photo_url)


##########################################################################
#                                                                                                                      #
# Interface d'administration des membres                                                                               #
#                                                                                                                      #
##########################################################################

@can_manage_membres
def admin_all_users(request) -> HttpResponse:
    """ Interface avec tous les utilisateurs du site

    Sont exclus les utilisateurs inactifs (dans le sens de Django) et le compte superuser (medialamouette)
    :param request:
    :return:
    """
    users = Utilisateur.objects.all().exclude(
        Q(actif=False) | Q(is_active=False)
    ).order_by('valide', 'groups','last_name')
    return render(request, 'admin_all_users.html', {'users': users})


@can_manage_membres
def admin_delete_user(request, uuid: int) -> HttpResponse:
    """ Désactive un compte utilisateur

    On désactive le compte à la place de le supprimer complètement pour garder l'historique de ses emprunts, les
    factures, etc (bref qui sont des clés étrangères d'autres modèles). En fin d'année, tous les comptes inactifs
    peuvent être définitivement supprimés.
    :param request:
    :param uuid: identifiant de l'utilisateur à supprimer
    :return:
    """
    user = get_object_or_404(Utilisateur, id=uuid)
    if not user.is_superuser:
        user.is_active = False
        user.save()
    return redirect('admin_all_users')

@staff_member_required
@login_required
def download_user_list(request) -> HttpResponse:
    """ Renvoie la liste des utilisateurs sont forme d'un tableur

    :param resquest:
    :return:
    """
    users = Utilisateur.objects.all()
    tab = []

    tab.append(['RAPPEL'])
    tab.append(['NON_PAYEE', 'PAYEE_CHQ','PAYEE_ESP', 'PAYEE_VIR', 'NON_NECESSAIRE'])
    tab.append(['ATT', 'PCH', 'PES', 'PVI', 'INU'])
    tab.append([])

    keys = [] #Contient l'ensemble des attributs de Utilisateur qui serviront de noms de colonnes
    unwanted_keys = ["is_superuser", "_state","last_login","id", "password"]
    for key in users[0].__dict__.keys():
        if key not in unwanted_keys \
            and type(users[0].__getattribute__(key)).__name__ != "FieldFile" \
            and type(users[0].__getattribute__(key)).__name__ != "ImageFieldFile":
                keys.append(key)
    othersKeys = ['Membre du CA', 'Ancien membre du CA'] #Autres colonnes
    columnTitle = keys + othersKeys #L'ensembles des titres de colonnes
    tab.append(columnTitle)

    membersCA = MembreCA.objects.all()

    for user in users:
        info = []
        for key in keys:
            att = user.__getattribute__(key)
            if type(att) is datetime:
                att = str(att.strftime("%Y-%m-%d"))
            if key == 'organisation_id' and type(att) == int:
                organisation = Organisation.objects.get(pk=att)
                att = organisation.nom
            if key == 'numeroChequeDeCaution' or key == 'anniversaire':
                att = str(att)
            info.append(att)

        isMemberCA = False
        for member in membersCA:
            if member.membre == user:
                isMemberCA = True
                if member.mandat.current is True:
                    info.append(member.statut.statut)
                    info.append("non")
                else:
                    info.append("non")
                    info.append(member.statut.statut)
                break

        if isMemberCA is False:
            info.append("non")
            info.append("non")

        tab.append(info)

    sheet = excel.pe.Sheet(tab)
    return excel.make_response(sheet, "xlsx", file_name="Liste des utilisateurs")

@can_manage_membres
def admin_user(request, uuid: int) -> HttpResponse:
    """ Interface de modification d'un utilisateur par le bureau

    :param request:
    :param uuid: identifiant de l'utilisateur à modifier
    :return:
    """
    utilisateur = get_object_or_404(Utilisateur, id=uuid)
    if request.method == 'POST':
        old_organisation = utilisateur.organisation
        form = AdminModificationForm(request.POST, request.FILES,
                                     instance=utilisateur)
        if form.is_valid():
            if old_organisation != form.cleaned_data.get(
                    'organisation') and form.cleaned_data.get('organisation'):
                Email('emails/changement_organisation.html', utilisateur.email,
                      {
                          'user': utilisateur,
                          'organisation': utilisateur.organisation
                      }).send()
            form.save()
            return redirect('admin_user', uuid=uuid)

    form = AdminModificationForm(instance=utilisateur)
    groups = Group.objects.all()
    return render(request, 'admin_user.html',
                  {'user': utilisateur, 'form': form, 'groups': groups,
                   'NON_PAYEE': Utilisateur.NON_PAYEE,
                   'PAYEE_ESP': Utilisateur.PAYEE_ESP,
                   'PAYEE_VIR': Utilisateur.PAYEE_VIR,
                   'PAYEE_CHQ': Utilisateur.PAYEE_CHQ,
                   'NON_NECESSAIRE': Utilisateur.NON_NECESSAIRE})


@can_manage_membres
def valider(request, uuid: int, groupid: int) -> HttpResponse:
    """ Permet de valider un compte utilisateur

    :param request:
    :param uuid: identifiant de l'utilisateur à valider
    :param groupid: identifiant du groupe qu'il veut rejoindre
    :return:
    """
    user = get_object_or_404(Utilisateur, id=uuid)
    group = get_object_or_404(Group, id=groupid)
    user.valide = True
    user.groups.clear()
    user.groups.add(group)
    if user.has_perm('utilisateur.fiche_inscription'):
        user.statutCotisation = Utilisateur.NON_PAYEE
    user.save()
    Email('emails/validation.html', user.email, locals()).send()
    return redirect('admin_user', uuid=uuid)


@can_manage_membres
def valider_cotisation(request, uuid: int, cotisid: str) -> HttpResponse:
    """ Valide le paiement de la cotisation de la part de l'utilisateur

    Après avoir valider le paiement, génère la fiche d'inscription de l'utilisateur
    :param request:
    :param uuid: identifiant de l'utilisateur
    :param cotisid: moyen de paiement de la cotisation
    :return:
    """
    user = get_object_or_404(Utilisateur, id=uuid)
    if cotisid in [Utilisateur.PAYEE_ESP, Utilisateur.PAYEE_VIR,
                   Utilisateur.PAYEE_CHQ]:
        user.statutCotisation = cotisid
        user.save()
        if user.has_perm('utilisateur.fiche_inscription'):
            generer_fiche_inscription(user)
    elif cotisid == Utilisateur.NON_NECESSAIRE:
        user.statutCotisation = cotisid
        user.save()
    return redirect('admin_user', uuid=uuid)


##########################################################################
#                                                                        #
# Fiches d'inscription et d'acceptation des CGU                          #
#                                                                        #
##########################################################################

def generer_fiche_inscription(user: Utilisateur):
    """ Génère la fiche d'inscription d'un utilisateur

    :param user: objet Utilisateur utilisé pour la génération de la fiche
    :return: void
    """
    today = datetime.now(timezone.utc)
    no_inscription = FicheInscription.next_id()
    filename = '{}_{}{}.pdf'.format(no_inscription,
                                    user.first_name.capitalize()[0],
                                    user.last_name.capitalize())
    fiche = FicheInscription(date=today.date(), utilisateur=user)
    fiche.fichier.save(filename, save_pdf('fiche_inscription.rml',
                                          {'user': user, 'today': today,
                                           'no_inscription': no_inscription,
                                           'PAYEE_ESP': Utilisateur.PAYEE_ESP,
                                           'PAYEE_VIR': Utilisateur.PAYEE_VIR,
                                           'PAYEE_CHQ': Utilisateur.PAYEE_CHQ}))
    fiche.save()


@login_required
def get_user_inscription(request, user_id: int):
    """
    Permet à l'utilisateur de récupérer sa propre fiche d'inscription. Le bureau peut récupérer les fiches de
    tous les utilisateurs

    :param request:
    :param user_id: identifiant de l'utilisateur dont on veut récupérer la fiche d'inscription
    :return:
    """
    user = get_object_or_404(Utilisateur, id=user_id)
    if user == request.user or request.user.is_staff:
        return media_response(user.ficheInscription.fichier.url)
    raise PermissionDenied


##########################################################################
#                                                                        #
# Actions de passation                                                   #
#                                                                        #
##########################################################################


@register(url='cgu/reinitialiser', name='reinitialiser_cgu',
          verbose_name="Redemander aux utilisateurs d'accepter les CGU",
          fa_icon="fas fa-file-contract", btn_class="btn-info delete")
@can_access_passation
def reinitialiser_cgu(request):
    Utilisateur.objects.reset_has_read_cgu()
    return redirect('passation')


@register(url='inactifs/supprimer', name='supprimer_anciens_membres',
          verbose_name="Supprimer les comptes des membres inactifs cette année",
          fa_icon="fas fa-user-slash", btn_class="btn-danger delete")
@can_access_passation
def supprimer_anciens_membres(request):
    """ Supprime tous les membres inactifs qui ne sont pas membres du bureau

    :param request:
    :return:
    """
    for user in get_user_model().objects.filter(actif=False, is_staff=False,
                                                is_superuser=False):
        user.delete()
    return redirect('passation')


@register(url='membres/desactiver', name='desactiver_membres',
          verbose_name="Désactiver les comptes des membres",
          fa_icon="fas fa-user-slash", btn_class="btn-danger delete")
@can_access_passation
def desactiver_membres(request) -> HttpResponse:
    """ Permet de désactiver tous les membres en fin d'année

    Rends les utilisateurs invalides, et réinitialise le statut de paiement de cotisation
    :param request:
    :return:
    """
    for user in get_user_model().objects.all():
        if not user.is_superuser and not user.is_staff:
            user.valide = False
            user.actif = False
        if user.statutCotisation != Utilisateur.NON_NECESSAIRE:
            user.statutCotisation = Utilisateur.NON_PAYEE
        user.save()
    return redirect('passation')


##########################################################################
#                                                                        #
# Conditions générales d'utilisation                                     #
#                                                                        #
##########################################################################


@login_required
@transaction.atomic
def accept_cgu(request):
    if request.method == 'POST':
        form = AcceptationCGUForm(request.POST)
        if form.is_valid():
            acceptation_cgu = form.save(commit=False)
            acceptation_cgu.utilisateur = request.user
            acceptation_cgu.save()
            acceptation_cgu.generate_fiche()

            request.user.has_read_cgu = True
            request.user.save()

            return HttpResponse(status=200)
        else:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=400)


@login_required
def get_user_cgu_consent(request, user_id: int):
    """
    Permet à l'utilisateur de récupérer sa propre fiche d'acceptation des CGU. Le bureau peut récupérer les fiches de
    tous les utilisateurs

    :param request:
    :param user_id: identifiant de l'utilisateur dont on veut récupérer la fiche d'acceptation des CGU
    :return:
    """
    if user_id != request.user.id and not request.user.is_staff and not request.user.is_superuser:
        raise PermissionDenied

    acceptationCGUs = AcceptationCGU.objects.filter(utilisateur__id=user_id)
    if not acceptationCGUs.exists():
        raise Http404

    return media_response(path=acceptationCGUs.latest('id').ficheCGU.url)

