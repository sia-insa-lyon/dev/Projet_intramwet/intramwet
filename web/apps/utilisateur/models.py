import os
from datetime import datetime, timezone, date
from tempfile import TemporaryFile
from typing import List

from django.contrib.auth.models import AbstractUser, Group, Permission, UserManager
from django.core.validators import RegexValidator
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.templatetags.static import static
from django.utils.functional import cached_property
from django_extensions.db.models import TimeStampedModel
from icalendar import vCalAddress, vText
from xhtml2pdf import pisa

from apps.conf.views import get_cgu_text


class UtilisateurManager(UserManager):
    def reset_has_read_cgu(self):
        for user in self.all():
            user.has_read_cgu = False
            user.save()


class Utilisateur(AbstractUser):
    anniversaire = models.DateField(
        'anniversaire',
        null=True,
        blank=True
    )
    adresse = models.TextField(
        'adresse',
        max_length=200,
        null=True,
        blank=True
    )
    numeroDeTelephoneRegex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Le numéro de téléphone doit être entré au format '+999999999'. 15 chiffres maximum."
    )
    numeroDeTelephone = models.CharField(
        'numéro de téléphone',
        validators=[numeroDeTelephoneRegex],
        max_length=17,
        null=True,
        blank=True
    )
    photo = models.ImageField(
        upload_to='photos_utilisateurs/',
        blank=True,
        null=True
    )
    montantCaution = models.IntegerField(
        'montant de la caution',
        default=0
    )
    numeroChequeDeCaution = models.IntegerField(
        'numero de cheque',
        blank=True,
        null=True
    )
    has_read_cgu = models.BooleanField(
        "Si l'utilisateur a accepté les CGU sur la période courante",
        default=False
    )
    valide = models.BooleanField(
        "membre validé par le bureau",
        default=False
    )
    actif = models.BooleanField(
        "membre actif pour l'année",
        default=True
    )

    NON_PAYEE = "ATT"
    PAYEE_ESP = "PES"
    PAYEE_CHQ = "PCH"
    PAYEE_VIR = "PVI"
    NON_NECESSAIRE = "INU"
    statutCotisation = models.CharField(
        max_length=3,
        choices=[
            (NON_PAYEE, "Non payée"),
            (PAYEE_ESP, "Payée en espèces"),
            (PAYEE_CHQ, "Payée par chèque"),
            (PAYEE_VIR, "Payée par virement"),
            (NON_NECESSAIRE, "Non nécessaire"),
        ],
        default=NON_NECESSAIRE,
    )

    organisation = models.ForeignKey(
        'treso.Client',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        related_name='utilisateurs'
    )

    objects = UtilisateurManager()

    def __str__(self):
        return "%s %s @%s" % (self.first_name, self.last_name, self.username)

    @cached_property
    def nomComplet(self):
        return "%s %s" % (self.first_name, self.last_name)

    @cached_property
    def photo_url(self):
        return self.photo.url if self.photo else static(
            'utilisateur/images/default-user.png')

    @cached_property
    def ficheInscription(self):
        today = datetime.now(timezone.utc)
        year = int(today.strftime('%Y'))
        if today.month < 9:
            debut = datetime(year=year - 1, month=9, day=1)
            fin = datetime(year=year, month=8, day=31, hour=23, minute=59,
                           second=59)
        else:
            debut = datetime(year=year, month=9, day=1)
            fin = datetime(year=year + 1, month=8, day=31, hour=23, minute=59,
                           second=59)
        return self.fichesInscription.all().filter(date__lt=fin,
                                                   date__gte=debut).order_by(
            '-date').first()

    @cached_property
    def vCalAddress(self):
        utilisateur = vCalAddress(f'MAILTO:{self.email}')
        utilisateur.params['cn'] = vText(self.nomComplet)
        return utilisateur

    @property
    def montant_cotisation(self) -> int:
        if self.statutCotisation != self.NON_NECESSAIRE:
            return CotisationGroupe.objects.get_for_user(user=self).cotisation
        return 0

    class Meta:
        permissions = (
            ('acceder_materiel', 'Peut accéder à la page materiel'),
            ('acceder_local', 'Peut accéder à la page d\'emprunt du local'),
            ('acceder_projets', 'Peut accéder à la page projets'),
            ('acceder_passation', "Peut accéder à l'interface de passation."),
            ('acceder_membres', "Peut accéder au module membres."),
            ('acceder_treso', "Peut accéder au module de tréso."),
            ('acceder_wiki', "Peut accéder au module wiki."),
            ('fiche_inscription',
             'Doit générer une fiche d\'inscription à la validation'),
            ('represente_organisation',
             'Peut représenter un client de l\'association'),
            ('acceder_demande_ndf', "Peut accéder à l'interface de saisie de note de frais."),
            ('recevoir_notification_demande_ndf', "Reçoit des notifications lorsqu'une nouvelle demande de note de frais est générée"),
            ('recevoir_notification_emprunt',
             "Reçoit des notifications lorsqu'une nouvelle demande d'emprunt est créée"),
            ('recevoir_notification_projet',
             "Reçoit des notifications lorsqu'un nouveau projet est créé")
        )


class StatutCA(models.Model):
    statut = models.CharField("Statut du membre au Conseil d'administration",
                              max_length=200)
    unique = models.BooleanField("Statut unique du poste CA", default=False)
    acceder_passation = models.BooleanField("Ce statut donne accès à l'interface de passation", default=False)
    acceder_treso = models.BooleanField("Ce statut donne accès à la tréso", default=False)
    acceder_su = models.BooleanField("Ce statut donne un accès super-utilisateur", default=False)
    notifications_projet = models.BooleanField("Ce statut est informé pour la création de nouveaux projets", default=False)
    notifications_emprunt = models.BooleanField("Ce statut est informé pour la création de nouveaux emprunts",
                                               default=False)

    PRESIDENT = "PRES"
    TRESORIER = "TRES"
    SECRETAIRE = "SECR"
    DEV = "DEV"
    AUTRE = "AUTR"
    GESTIONNAIRE_CINET = "CINT"
    poste = models.CharField(
        max_length=4,
        choices=[
            (PRESIDENT, "Président"),
            (TRESORIER, "Trésorier"),
            (SECRETAIRE, "Sécretaire"),
            (DEV, "Développeur"),
            (AUTRE, "Autre"),
            (GESTIONNAIRE_CINET, "Gestionnaire cinet'")
        ],
        default=AUTRE,
    )

    def __str__(self):
        return "%s" % self.statut


class MandatCAManager(models.Manager):
    def current(self):
        current_cas = self.filter(Q(fin=None) | Q(fin__gt=datetime.today().date()))
        if current_cas:
            return current_cas.latest('id')
        return self.none()


class MandatCA(models.Model):
    debut = models.DateField("Date de début du mandat", auto_now_add=True)
    fin = models.DateField("Date de fin du mandat", null=True, blank=True)
    objects = MandatCAManager()

    @property
    def current(self) -> bool:
        return self.fin is None or self.fin > datetime.today().date()

    def __str__(self):
        return "Bureau actuel" if self.current else f"Du {self.debut} au {self.fin}"


@receiver(post_save, sender=MandatCA)
def remove_admin_rights_for_old_members(sender, instance: MandatCA,
                                        created: bool, **kwargs):
    for m in instance.membresCA.all():
        m.save(force_update=True)


class MembreCAManager(models.Manager):
    def emails_for_permission(self, permission_codename: str):
        perm = Permission.objects.get(codename=permission_codename)
        curr_mandat = MandatCA.objects.current()
        queryset = self.filter(mandat=curr_mandat) if curr_mandat else self
        emails_tuple_list = queryset.filter(
            Q(membre__groups__permissions=perm) | Q(membre__user_permissions=perm)).distinct().values_list(
            'membre__email')
        if emails_tuple_list:
            emails = [e[0] for e in
                      emails_tuple_list]  # django's values_list method returns a (email,) tuple for each entry, converting to list
            return list(set(emails))  # returning unique emails
        return []

    def emails_to_notify_for_emprunt(self) -> List[str]:
        return self.emails_for_permission('recevoir_notification_emprunt')

    def emails_to_notify_for_projet(self) -> List[str]:
        return self.emails_for_permission('recevoir_notification_projet')

    def emails_to_notify_for_ndf(self) -> List[str]:
        return self.emails_for_permission('recevoir_notification_demande_ndf')


class MembreCA(models.Model):
    statut = models.ForeignKey(StatutCA, on_delete=models.PROTECT)
    membre = models.ForeignKey(Utilisateur, on_delete=models.CASCADE,
                               related_name="postesCA")
    mandat = models.ForeignKey(MandatCA, on_delete=models.CASCADE,
                               related_name="membresCA")
    objects = MembreCAManager()

    @property
    def current(self) -> bool:
        return self.mandat.current

    @cached_property
    def notification_ndf(self) -> bool:
        return self.membre.has_perm('utilisateur.recevoir_notification_demande_ndf')

    def set_admin_rights(self, save: bool = True):
        self.membre.is_staff = True
        self.membre.is_superuser = self.statut.acceder_su
        permission_codenames = []
        if self.statut.acceder_passation:
            permission_codenames.append('acceder_passation')
        if self.statut.acceder_treso:
            permission_codenames.extend(['acceder_treso', 'recevoir_notification_demande_ndf'])
        if self.statut.notifications_projet:
            permission_codenames.append('recevoir_notification_projet')
        if self.statut.notifications_emprunt:
            permission_codenames.append('recevoir_notification_emprunt')
        self.membre.user_permissions.add(*[Permission.objects.get(codename=p).id for p in permission_codenames])
        self.membre.save()
        if save:
            self.save()

    def remove_admin_rights(self, save: bool = True):
        if not self.membre.is_superuser:
            self.membre.is_staff = False
            self.membre.is_superuser = False
            permission_ids_to_remove = []
            for p in ['acceder_passation', 'acceder_treso', 'recevoir_notification_demande_ndf', 'recevoir_notification_emprunt', 'recevoir_notification_projet']:
                if self.membre.has_perm(f'utilisateur.{p}'):
                    permission_ids_to_remove.append(Permission.objects.get(codename=p).id)
            self.membre.user_permissions.remove(*permission_ids_to_remove)
            self.membre.save()
            if save:
                self.save()

    def __str__(self):
        return "%s | %s" % (self.statut, self.membre)


@receiver(post_save, sender=MembreCA)
def set_admin_rights_on_membre_CA_saved(sender, instance: MembreCA,
                                        created: bool, **kwargs):
    if instance.mandat.current:
        instance.set_admin_rights(save=False)
    else:
        instance.remove_admin_rights(save=False)


class DescriptionGroupe(models.Model):
    group = models.OneToOneField(Group, on_delete=models.CASCADE,
                                 related_name='description')
    description = models.TextField("description du groupe", max_length=200)
    demandePrecisions = models.BooleanField(
        "nécessite des précisions supplémentaires", default=False)

    def __str__(self):
        return self.group.name


class CotisationGroupeManager(models.Manager):
    def get_for_user(self, user: Utilisateur):
        return self.filter(group__in=user.groups.all()).order_by('cotisation').first()


class CotisationGroupe(models.Model):
    group = models.OneToOneField(Group, on_delete=models.CASCADE,
                                 related_name='cotisation')
    cotisation = models.IntegerField(default=0)
    objects = CotisationGroupeManager()

    def __str__(self):
        return self.group.name


class FicheInscription(models.Model):
    utilisateur = models.ForeignKey(Utilisateur, on_delete=models.CASCADE,
                                    related_name='fichesInscription')
    fichier = models.FileField(upload_to='fiches_inscription/')
    date = models.DateField('Date de génération')

    def __str__(self):
        return "%s | %s" % (
            self.utilisateur.nomComplet, os.path.basename(self.fichier.name))

    @staticmethod
    def next_id():
        today = datetime.now(timezone.utc)
        if today.month < 9:
            debut = datetime(year=today.year - 1, month=9, day=1)
            fin = datetime(year=today.year, month=8, day=31, hour=23,
                           minute=59, second=59)
        else:
            debut = datetime(year=today.year, month=9, day=1)
            fin = datetime(year=today.year + 1, month=8, day=31, hour=23,
                           minute=59, second=59)
        return "I{}{}{:0>3}".format(debut.strftime('%y'), fin.strftime('%y'),
                                    len(FicheInscription.objects.filter(
                                        date__lt=fin, date__gte=debut)) + 1)


class AcceptationCGU(TimeStampedModel):
    utilisateur = models.ForeignKey(
        Utilisateur,
        on_delete=models.SET_NULL,
        null=True,
        related_name='acceptationCGU'
    )
    ficheCGU = models.FileField(
        upload_to='cgu/',
        null=True,
        blank=True
    )
    has_accepted_cgu = models.BooleanField(
        verbose_name="A accepté les CGU ?",
        default=False
    )
    has_accepted_rgpd = models.BooleanField(
        verbose_name="A accepté la politique de confidentialité ?",
        default=False
    )
    has_accepted_cookies = models.BooleanField(
        verbose_name="A accepté les cookies ?",
        default=False
    )

    def generate_fiche(self):
        assert self.utilisateur is not None
        today = datetime.now(timezone.utc)
        year = int(today.strftime('%y'))
        no_inscription = "I{}{}{:0>3}".format(
            year - 1 if today.month < 9 else year,
            year if today.month < 9 else year + 1,
            self.utilisateur.id)
        filename = 'CGU_{}_{}{}.pdf'.format(no_inscription,
                                            self.utilisateur.first_name.capitalize()[0],
                                            self.utilisateur.last_name.capitalize())
        with TemporaryFile(mode='w+b') as f:
            html_data = render_to_string('fiche_cgu.html',
                                         {'user': self.utilisateur, 'today': today,
                                          'no_inscription': no_inscription,
                                          'cgu': get_cgu_text()})
            pisa.CreatePDF(html_data.encode('utf-8'), dest=f, encoding='utf-8')
            f.seek(0)
            self.ficheCGU.save(filename, f)
            self.save()
