# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Categorie, Document, Modification, Image


@admin.register(Categorie)
class CategorieAdmin(admin.ModelAdmin):
    list_display = ('nom',)


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('titre', 'categorie', 'contenu')
    list_filter = ('categorie',)


@admin.register(Modification)
class ModificationAdmin(admin.ModelAdmin):
    list_display = ('membre', 'document', 'date')
    list_filter = ('membre', 'document', 'date')


@admin.register(Image)
class ImageAdmin(admin.ModelAdmin):
    list_display = ('image',)
