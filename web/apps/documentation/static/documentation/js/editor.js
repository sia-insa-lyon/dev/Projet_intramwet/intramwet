$(document).ready(function () {
    var simplemde = new SimpleMDE({
        forceSync: true,
    });

    $('#modal').on('show.bs.modal', function (event) {
        $.ajax({
            url: url_chargement_image,
            dataType: 'html',
            success: function (data) {
                $('#modal .modal-content').html(data);
            }
        });
    });
});