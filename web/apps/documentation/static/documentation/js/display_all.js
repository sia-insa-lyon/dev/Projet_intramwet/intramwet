$(document).ready(function () {
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]'
    });

    $('.switch').click(function () {
        $(this).find('i').toggleClass("fa-angle-right fa-angle-down");
    });
});