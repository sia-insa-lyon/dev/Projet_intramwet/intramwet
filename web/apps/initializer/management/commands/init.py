from django.core.management import call_command
from django.core.management.base import BaseCommand
from ...initialization_handler import initialization_handler


class Command(BaseCommand):
    help = "Initilizes the database using each app's init module."

    def add_arguments(self, parser):
        parser.add_argument('-b', '--blank', action='store_true', help='Blank run')

    def handle(self, *args, **kwargs):
        call_command('migrate', no_input=True)

        initialization_handler.run_initialization(blank=kwargs['blank'])
