import inspect
from abc import ABC
from apps.initializer.models import InitializationJob


class BaseInitializer(ABC):
    priority = 1

    @property
    def init_methods(self):
        attrs = (getattr(self, name) for name in dir(self))
        return filter(lambda a: inspect.ismethod(a) and a.__name__.startswith("init"), attrs)

    def run(self, blank: bool = False):
        for method in self.init_methods:
            if not InitializationJob.objects.exists(method):
                try:
                    print(f"Running {method.__name__} from {self.__class__.__name__}... ", end="")
                    if not blank:
                        method()
                    InitializationJob.objects.create_job(method)
                    print('\x1b[6;30;42m' + 'Success!' + '\x1b[0m')
                except TypeError:
                    raise Exception("Too many arguments : an initialization method can't have any arguments.")
            else:
                print(f"{method.__name__} from {self.__class__.__name__} has already been executed, skipping!")
