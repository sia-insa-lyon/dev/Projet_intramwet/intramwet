from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.utils.functional import cached_property

from apps.conf.utils import format_file_title
from . import Client
from .utils import FactureAbstraite, FactureElementAbstrait


class Facture(FactureAbstraite):
    """Modèle de base pour les factures

    Il s'agit de la classe mère de tous les types de factures.
    Une facture est composée de FactureElements, liés à des Prestations
    """
    client = models.ForeignKey(Client, on_delete=models.PROTECT,
                               related_name="factures")
    fichier = models.FileField(upload_to='factures/', null=True, blank=True)

    @cached_property
    def nom_fichier(self):
        return format_file_title('F{}{}.pdf'.format(self.numero, '_{}'.format(
            self.label) if self.label else ''))

    @cached_property
    def total(self):
        return Facture.calc_total(self.elements.all())


class FactureElement(FactureElementAbstrait):
    facture = models.ForeignKey(Facture, on_delete=models.CASCADE,
                                related_name='elements')

    def __str__(self):
        return "%s | %s | %d€ | %d" % (
            self.facture, self.nom, self.prix, self.quantite)


@receiver(post_delete, sender=Facture)
def remove_file_on_delete(sender, instance: Facture, **kwargs):
    instance.fichier.delete(False)
