from django.db import models
from django.utils import timezone
from django.utils.functional import cached_property

from apps.conf.utils import get_schoolyear


class TresoAbstraite(models.Model):
    numero = models.IntegerField("Numéro", unique=True)
    date = models.DateField(default=timezone.now)
    fichier = models.FileField(upload_to='treso/', null=True, blank=True)

    @staticmethod
    def get_prefix():
        return get_schoolyear() * 1000

    @classmethod
    def dernier_numero(cls):
        return cls.objects.order_by('-numero')[
            0].numero if cls.objects.all() else cls.get_prefix()

    class Meta:
        abstract = True


class FactureAbstraite(TresoAbstraite):
    label = models.CharField("Libellé court", max_length=100, null=True,
                             blank=True)
    motif = models.CharField("Motif complet", max_length=200, null=True,
                             blank=True)

    @staticmethod
    def calc_total(elements):
        return sum(e.montant for e in elements) if elements else 0

    def __str__(self):
        return "%d | %s" % (self.id, self.label)

    class Meta:
        abstract = True


class FactureElementAbstrait(models.Model):
    """Une entrée de Facture

    Le coefficient multiplicatif permet d'appliquer des réductions (notamment pour les associations)
    """
    nom = models.CharField("Nom de la prestation", max_length=200)
    quantite = models.IntegerField("Quantité")
    prix = models.FloatField("Prix de la prestation")
    coefficient = models.FloatField("Coefficient multiplicatif", default=1.0)

    @cached_property
    def montant(self):
        return self.quantite * self.prix * self.coefficient

    class Meta:
        abstract = True
