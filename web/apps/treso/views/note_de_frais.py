from django.db.models import ProtectedError
from django.http import HttpResponse
from django.shortcuts import redirect, render, get_object_or_404

from apps.conf.utils import get_zipped_response, get_schoolyear, media_response
from ..decorators import can_manage_treso, can_access_demande_ndf
from ..forms import NoteDeFraisForm, NoteDeFraisElementFormset
from ..forms.note_de_frais import DemandeNoteDeFraisForm, NoteDeFraisElementFormsetEdit, NoteDeFraisEditForm
from ..models import NoteDeFrais, NoteDeFraisElement
from . import save_pdf, render_pdf_response
from apps.passation.decorators import can_access_passation, register
from ..utils import notify_new_ndf


@can_manage_treso
def note_de_frais(request, sauvegarder: bool = False,
                  id_tache: int = None) -> HttpResponse:
    """ Permet de générer une note de frais, éventuellement reliée à une tache de projet

    Utilise les Formset Django pour relier les éléments à la note de frais (https://medium.com/@taranjeet/adding-forms-dynamically-to-a-django-formset-375f1090c2b0)
    :param request:
    :return:
    """
    template_name = "treso/note_de_frais.html"
    if request.method == "POST":
        ndfform = NoteDeFraisForm(request.POST,request.FILES)
        formset = NoteDeFraisElementFormset(request.POST,request.FILES)
        if ndfform.is_valid() and formset.is_valid():
            ndf = ndfform.save(commit=sauvegarder)
            elements = []
            for form in formset:
                if sauvegarder:
                    element = form.save(commit=False)
                    element.ndf = ndf
                    element.save()

                else:
                    elements.append({
                        "frais": form.cleaned_data.get("frais"),
                        "montant": form.cleaned_data.get("montant")
                    })

            if sauvegarder:
                ndf.fichier.save(ndf.nom_fichier,
                                 save_pdf('treso/rml/note_de_frais.rml',
                                          locals()))
                return redirect("generer_note_de_frais", ndf.id)
            else:
                total = sum(e["montant"] for e in elements)
                return render_pdf_response(
                    'treso/rml/note_de_frais_sans_sauvegarde.rml',
                    ndf.nom_fichier, locals())
    else:
        ndfform = NoteDeFraisForm(request.GET or None)
        formset = NoteDeFraisElementFormset(queryset=NoteDeFraisElement.objects.none())
    return render(request, template_name, locals())


@can_manage_treso
def detail_note_de_frais(request, id_ndf: int) -> HttpResponse:
    template_name = "treso/detail_note_de_frais.html"
    ndf = get_object_or_404(NoteDeFrais, id=id_ndf)

    if request.method == "POST":
        ndfform = NoteDeFraisEditForm(request.POST, instance=ndf)
        formset = NoteDeFraisElementFormsetEdit(request.POST, queryset=ndf.elements.all())
        if ndfform.is_valid() and formset.is_valid():
            ndf = ndfform.save()
            ndf_elements = formset.save(commit=False)
            for e in ndf_elements:
                if e.ndf_id is None:
                    e.ndf = ndf
                    e.save()
            ndf.refresh_from_db()
            ndf.fichier.save(ndf.nom_fichier, save_pdf('treso/rml/note_de_frais.rml', locals()))
            return redirect("treso")
    else:
        ndfform = NoteDeFraisEditForm(instance=ndf)
        formset = NoteDeFraisElementFormsetEdit(queryset=ndf.elements.all())
    return render(request, template_name, locals())


@can_access_demande_ndf
def demande_note_de_frais(request) -> HttpResponse:
    """ Permet de générer une note de frais, éventuellement reliée à une tache de projet

    Utilise les Formset Django pour relier les éléments à la note de frais (https://medium.com/@taranjeet/adding-forms-dynamically-to-a-django-formset-375f1090c2b0)
    :param request:
    :return:
    """
    template_name = "treso/demande_note_de_frais.html"
    ndfs = NoteDeFrais.objects.filter(collaborateur=request.user)
    if request.method == "POST":
        ndfform = DemandeNoteDeFraisForm(request.POST, request.FILES)
        formset = NoteDeFraisElementFormset(request.POST)
        if ndfform.is_valid() and formset.is_valid():
            ndf = ndfform.save(collaborateur=request.user)

            elements = []
            for form in formset:
                element = form.save(commit=False)
                element.ndf = ndf
                element.save()

            notify_new_ndf(request=request, ndf=ndf)
            return redirect("accueil")
    else:
        ndfform = DemandeNoteDeFraisForm()
        formset = NoteDeFraisElementFormset(queryset=NoteDeFraisElement.objects.none())
    return render(request, template_name, locals())


@can_manage_treso
def generer_note_de_frais(request, id_ndf: int) -> HttpResponse:
    """ Permet de générer le PDF associé à une note de frais déjà sauvegardée

    :param request:
    :param id_ndf: id de la note de frais
    :return: le PDF de la note de frais
    """
    ndf = get_object_or_404(NoteDeFrais, id=id_ndf)
    return render_pdf_response('treso/rml/note_de_frais.rml', ndf.nom_fichier,
                               locals())


@can_manage_treso
def accepter_note_de_frais(request, id_ndf: int) -> HttpResponse:
    try:
        ndf = get_object_or_404(NoteDeFrais, id=id_ndf)
        ndf.valider()
    except ProtectedError:
        pass
    return redirect('treso')


@can_manage_treso
def refuser_note_de_frais(request, id_ndf: int) -> HttpResponse:
    try:
        ndf = get_object_or_404(NoteDeFrais, id=id_ndf)
        ndf.refuser()
    except ProtectedError:
        pass
    return redirect('treso')


@can_manage_treso
def supprimer_note_de_frais(request, id_ndf: int) -> HttpResponse:
    """ Permet de supprimer une note de frais

    :param request:
    :param id_ndf: l'id de la note de frais à supprimer
    :return:
    """

    try:
        ndf = get_object_or_404(NoteDeFrais, id=id_ndf)
        ndf.delete()
    except ProtectedError:
        pass
    return redirect('treso')


@can_manage_treso
def get_note_de_frais(request, ndf_id: int):
    """
    Permet au bureau de récupérer une note de frais

    :param request:
    :param ndf_id: id de l'objet NoteDeFrais à télécharger
    :return:
    """
    ndf = NoteDeFrais.objects.get(id=ndf_id)
    return media_response(ndf.fichier.url)


@can_manage_treso
def telecharger_notes_de_frais(request) -> HttpResponse:
    files = [f.fichier.path for f in NoteDeFrais.objects.all() if f.fichier]
    return get_zipped_response(file_list=files,
                               archive_name=f'{get_schoolyear()}_notes_de_frais.zip')


@register(url='ndf/supprimer', name='supprimer_ndf_toutes',
          verbose_name="Supprimer toutes les notes de frais",
          fa_icon="fas fa-trash", btn_class="btn-danger delete")
@can_access_passation
def supprimer_ndfs(request):
    """ Supprime toutes les notes de frais

    :param request:
    :return:
    """
    for ndf in NoteDeFrais.objects.all():
        ndf.delete()
    return redirect('passation')