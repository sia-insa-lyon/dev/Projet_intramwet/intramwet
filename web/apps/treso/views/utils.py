from typing import Any, Dict

from django.contrib.staticfiles.finders import find
from django.core.files.base import ContentFile
from django.http import HttpResponse
from django.template.loader import get_template
from trml2pdf import parseString

from ..forms import FactureForm
from ...conf.context_processors import intramwet_settings


def traiter_facture(factureform: FactureForm, formset, sauvegarder: bool,
                    devis: bool = False):
    if factureform.is_valid() and formset.is_valid():
        facture = factureform.save(commit=sauvegarder)

        elements = []
        for form in formset:
            if sauvegarder:
                element = form.save(commit=False)
                element.facture = facture
                element.save()
            else:
                elements.append(
                    {'prestation': form.cleaned_data.get('nom'),
                     'quantite': form.cleaned_data.get('quantite'),
                     'prix': form.cleaned_data.get('prix'),
                     'coefficient': form.cleaned_data.get('coefficient')})

        if sauvegarder:
            facture.fichier.save(facture.nom_fichier,
                                 save_pdf('treso/rml/facture.rml', locals()))
            return render_pdf_response('treso/rml/facture.rml',
                                       facture.nom_fichier, locals())
        else:
            total = sum(
                e['quantite'] * e['prix'] * e['coefficient'] for e in elements)
            return render_pdf_response('treso/rml/facture_sans_sauvegarde.rml',
                                       facture.nom_fichier, locals())


def render_pdf(file: str, data: Dict[str, Any]) -> bytes:
    """ Génère le PDF associé au modèle spécifié

    :param file: le nom du template
    :param data: les données
    :return: le PDF généré
    """
    t = get_template(file)
    params = intramwet_settings()
    data_with_context = {**params, **data}
    data_with_context['ADDRESS'] = data_with_context['ADDRESS'].splitlines()
    rml = t.render(data_with_context)
    rml.encode('utf8')
    return parseString(rml)


def render_pdf_response(file: str, title: str,
                        data: Dict[str, Any]) -> HttpResponse:
    """ Génère un PDF et l'englobe dans une réponse HTTP pour le renvoyer à l'utilisateur

    :param file: le nom du template
    :param title: titre à donner au PDF généré
    :param data: les données
    :return: la réponse HTTP contenant le PDF généré
    """

    pdf = render_pdf(file, data)
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'filename="{}"'.format(title)
    response.write(pdf)
    return response


def save_pdf(file: str, data: Dict[str, Any]) -> ContentFile:
    """ Génère le PDF demandé et l'englobe dans un objet ContentFile (pour pouvoir le sauvegarder dans la BD)

    :param file: le nom du template
    :param data: les données
    :return: l'objet ContentFile à sauvegarder dans la BD
    """
    return ContentFile(render_pdf(file, data))
