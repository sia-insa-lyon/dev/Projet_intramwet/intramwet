from .utils import save_pdf, render_pdf_response, traiter_facture
from .clients import liste_client, edit_client, creer_client, supprimer_client, \
    mon_organisation, \
    demande_organisation, generer_organisation_client_particulier
from .devis import devis, generer_devis, supprimer_devis, telecharger_devis, supprimer_devis_tous
from .facture import facture, generer_facture, supprimer_facture, \
    telecharger_factures, get_facture, supprimer_factures
from .facture_emprunt import facture_emprunt, emprunts
from .grille_tarifaire import grille_tarifaire, modifier_entree_grille, \
    supprimer_entree_grille
from .note_de_frais import note_de_frais, generer_note_de_frais, \
    supprimer_note_de_frais, telecharger_notes_de_frais, get_note_de_frais, supprimer_ndfs, demande_note_de_frais
from .treso import treso
