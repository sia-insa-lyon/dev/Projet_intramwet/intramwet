from apps.conf.utils import get_prefix
from .models import NoteDeFrais


def notify_new_ndf(request, ndf: NoteDeFrais):
    from ..conf.bot import Email
    from ..utilisateur.models import MembreCA

    prefix = get_prefix(request)
    for email in MembreCA.objects.emails_to_notify_for_ndf():
        Email('emails/notification_demande_ndf_bureau.html', email, locals()).send()
