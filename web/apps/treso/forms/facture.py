from django import forms
from django.forms import modelformset_factory

from apps.conf.utils import get_schoolyear, format_file_title
from ..models import Client, Facture, FactureElement


class FactureForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(FactureForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
        self.fields['date'].widget.attrs.update(
            {'data-target': '#id_date', 'data-toggle': 'datetimepicker',
             'value': ''})
        self.fields['motif'].widget.attrs.update(
            {'placeholder': 'Objet (Ex: Captation TE 2019)'})
        self.fields['label'].widget.attrs.update(
            {'placeholder': '(Facultatif) (Ex: Capta_TE)'})
        self.fields['client'].queryset = Client.objects.all().order_by(
            'particulier')
        if 'numero' in self.fields:
            self.fields['numero'].widget.attrs.update(
                {'placeholder': '%4d042' % (get_schoolyear()),
                 'min': '1010001', 'max': '9999999'})

    def clean_label(self):
        return format_file_title(self.cleaned_data['label'])

    class Meta:
        model = Facture
        fields = ['numero', 'motif', 'label', 'client', 'date']
        help_texts = {
            'numero': "Numéro de la facture au format %4dXXX (sans préfixe F)" % (
                get_schoolyear()),
            'date': "Date d'édition de la facture",
            'label': 'Nom court sans espace donné au fichier PDF',
            'motif': 'Motif imprimé sur la facture',
        }


FactureElementFormset = modelformset_factory(
    FactureElement,
    fields=('nom', 'quantite', 'prix', 'coefficient'),
    extra=1
)
