from django import forms

from ..models import EntreeGrilleTarifaire


class EntreeGrilleTarifaireForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EntreeGrilleTarifaireForm, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({'class': 'form-control'})
        self.fields['nom'].widget.attrs.update({'placeholder': 'Nom'})
        self.fields['description'].widget.attrs.update(
            {'placeholder': 'Description (Facultatif)'})
        self.fields['prix'].widget.attrs.update(
            {'placeholder': '0.00€', 'step': '0.01'})

    class Meta:
        model = EntreeGrilleTarifaire
        fields = ['nom', 'description', 'prix', 'categorie']
        help_texts = {
            'nom': "Nom de la prestation à imprimer sur la facture",
            'description': "Description pour le bureau de la prestation",
            'prix': "Prix d'une prestation unitaire (en euros). Peut être négatif s'il s'agit d'une réduction.",
        }
