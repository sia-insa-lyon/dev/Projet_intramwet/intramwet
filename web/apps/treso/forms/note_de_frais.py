from django import forms
from django.core.exceptions import ValidationError
from django.forms import modelformset_factory

from apps.conf.utils import get_schoolyear
from ..models import NoteDeFrais, NoteDeFraisElement
from ...utilisateur.models import Utilisateur


class NoteDeFraisForm(forms.ModelForm):
    def save(self, commit: bool = True):
        ndf = super().save(commit=False)
        ndf.statut = NoteDeFrais.EnumStatut.VALIDEE
        ndf.numero = NoteDeFrais.dernier_numero() + 1
        if commit:
            ndf.save()
        return ndf

    def clean(self):
        cleaned_data = super().clean()
        methode_remboursement = cleaned_data.get("methode_remboursement")
        numero_cheque = cleaned_data.get("numero_cheque")

        if numero_cheque is not None and numero_cheque != "" and methode_remboursement != NoteDeFrais.CHQ:
            raise ValidationError(
                "Vous ne pouvez spécifier un numéro de chèque que si la note de frais est remboursée par chèque."
            )

    class Meta:
        model = NoteDeFrais
        fields = ['collaborateur', 'nature', 'date',
                  'methode_remboursement', 'numero_cheque', 'justificatif']
        help_texts = {
            'numero': "Numéro de la note de frais au format %4dXXX (sans préfixe N)" % (
                get_schoolyear()),
            'date': "Date d'édition de la note de frais",
            'nature': 'Nature des frais remboursés',
            'numero_cheque': 'Seulement si remboursée par chèque'
        }


class DemandeNoteDeFraisForm(forms.ModelForm):
    def save(self, collaborateur: Utilisateur):
        demande = super().save(commit=False)
        demande.collaborateur = collaborateur
        demande.statut = NoteDeFrais.EnumStatut.ATTENTE
        demande.numero = NoteDeFrais.dernier_numero() + 1
        demande.save()
        return demande

    class Meta:
        model = NoteDeFrais
        fields = ['nature', 'date', 'methode_remboursement', 'justificatif']
        help_texts = {
            'date': "Date d'édition de la note de frais",
            'nature': 'Nature des frais remboursés',
        }


class NoteDeFraisEditForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super().clean()
        methode_remboursement = cleaned_data.get("methode_remboursement")
        numero_cheque = cleaned_data.get("numero_cheque")

        if numero_cheque is not None and numero_cheque != "" and methode_remboursement != NoteDeFrais.CHQ:
            raise ValidationError(
                "Vous ne pouvez spécifier un numéro de chèque que si la note de frais est remboursée par chèque."
            )

    class Meta:
        model = NoteDeFrais
        fields = ['collaborateur', 'nature', 'date',
                  'methode_remboursement', 'numero_cheque', 'statut']
        help_texts = {
            'date': "Date d'édition de la note de frais",
            'nature': 'Nature des frais remboursés',
            'numero_cheque': 'Seulement si remboursée par chèque'
        }


NoteDeFraisElementFormset = modelformset_factory(
    NoteDeFraisElement,
    fields=('frais', 'montant'),
    extra=1
)

NoteDeFraisElementFormsetEdit = modelformset_factory(
    NoteDeFraisElement,
    fields=('frais', 'montant', 'id'),
    extra=0
)
