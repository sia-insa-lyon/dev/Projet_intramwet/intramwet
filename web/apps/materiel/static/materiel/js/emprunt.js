let form;
$(function () {
    // Slider de catégories
    $('.switch').click(function () {
        $(this).find('i').toggleClass("fa-angle-right fa-angle-down");
    });

    if ($(window).width() < 992) {
        $('.collapse').each(function () {
            $(this).removeClass("show");
        });
        $('.switch').each(function () {
            $(this).find('i').toggleClass("fa-angle-right fa-angle-down");
        });
    }

    // Plugin de bouton de confirmation
    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]'
    });

    // Configuration des pickers de dates
    let picker_options = {
        locale: 'fr',
        format: 'DD/MM/YYYY HH:mm',
        date: null,
    };
    $('#id_debut').datetimepicker(picker_options);
    $('#id_fin').datetimepicker(picker_options);

    $('.quantite-materiel').click(function () {
        if($('#id_fin').val() == "" || $('#id_fin').val() == ""){
            $('#alertes-no-date').show();
        } else {
            $('#alertes-no-date').hide();
        }
    });

    $('#id_debut, #id_fin').on('change.datetimepicker', function (e) {
        // Date par défaut au premier clic sur le picker
        if (e.oldDate === null) {
            $(this).datetimepicker('date', moment().add(($(this).is('#id_debut') ? 3 : 4), 'days').hours(13).minutes(0));
        }

        if (!moment($(this).val(), 'DD/MM/YYYY HH:mm').isValid()) {
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
            let is_valid = true;
            let debut = moment($('#id_debut').val(), 'DD/MM/YYYY HH:mm');
            let fin = moment($('#id_fin').val(), 'DD/MM/YYYY HH:mm');

            if (debut.isValid() && fin.isValid()) {
                 $('#alertes-no-date').hide();
                if (debut.isSame(fin)) {
                    $('#alerte-dates-egales').show();
                    is_valid = false;
                } else {
                    $('#alerte-dates-egales').hide();
                }

                if (debut.isAfter(fin)) {
                    $('#alerte-dates-inversees').show();
                    is_valid = false;
                } else {
                    $('#alerte-dates-inversees').hide();
                }

                if (moment().isAfter(debut) || moment().isAfter(fin)) {
                    $('#alerte-dates-anciennes').show();
                    is_valid = false;
                } else {
                    $('#alerte-dates-anciennes').hide();
                }

                if (is_valid) {
                    check_dispo();
                }
            }
        }
    });

    $('#raison textarea').blur(function () {
        if ($(this).val() === '') {
            $(this).addClass('is-invalid');
        } else {
            $(this).removeClass('is-invalid');
        }
    });

    form = $('#demande-emprunt');
    form.validate({
        rules: {
            raison: {
                required: false
            }
        }
    });
});

function check_dispo() {
    if (form.valid()) {
        $.ajax({
            url: url_emprunt_disponibilite,
            type: 'post',
            data: $(form).serialize(),
            success: function (data) {
                for (let id in data) {
                    if (data[id]['dispo']) {
                        $("#row-" + id).removeClass("non-disponible");
                        $("#row-" + id + " button").removeClass("disabled");
                        $('#quantite-restante-' + id).text(' (' + data[id]['quantite'] + ' restant' + ((data[id]['quantite'] > 1) ? 's' : '') + ')');
                        $('#quantite-restante-' + id).show();
                        let q = parseInt($('#quantite-' + id).val());
                        $('#quantite-' + id).html('');
                        for (let i = 1; i < data[id]['quantite'] + 1; i++) {
                            if (q == i)
                                $('#quantite-' + id).append($('<option selected value="' + i + '">' + i + '</option>'));
                            else
                                $('#quantite-' + id).append($('<option value="' + i + '">' + i + '</option>'));
                        }
                    } else {
                        $("#row-" + id).addClass("non-disponible");
                        $("#row-" + id + " button").addClass("disabled");

                        let input = $("input[value='" + id + "']");
                        let bouton = $("button[name='" + id + "']");
                        input.prop('checked', false);
                        bouton.addClass('btn-primary');
                        bouton.removeClass('btn-success');
                        $("img[name='" + id + "']").removeClass('photo-selected');
                        $("#row-" + id).parent().removeClass('card-active');
                    }
                }
                if ($.isEmptyObject(data)) {
                    $('.date input').addClass('is-invalid');
                }
            }
        });
    } else {
        $('.date input').addClass('is-invalid');
    }
}

function show_more(id) {
    $.ajax({
        url: url_materiel_description,
        dataType: 'json',
        data: {'id': id},
        method: 'GET',
        success: (data) => {
            $('#matos-description-' + id).text(data['description']);
        }
    });
}

function reserver(id) {
    let bouton = $("button[name='" + id + "']");
    if (!bouton.hasClass("disabled")) {
        let input = $("input[value='" + id + "']");
        input.prop('checked', !input.prop('checked'));
        bouton.toggleClass('btn-primary');
        bouton.toggleClass('btn-success');
        $("img[name='" + id + "']").toggleClass('photo-selected');
        $("#row-" + id).parent().toggleClass('card-active');
    }
}

function envoyer() {
    let form = $("form");
    if (form[0].checkValidity()) {
        form.ajaxSubmit({
            url: form.action,
            type: 'POST',
            success: function (response) {
                if (response['valid'] === true) {
                    let next = $('#next').val();
                    location.replace(next);
                } else {
                    let alert_field = $("#alerts");
                    alert_field.html(response['alerts']);
                    alert_field.css('display', 'block');
                    scrollTo(0, 0);
                }
            }
        });
    } else form[0].reportValidity();
}

function isEmpty(str) {
    return str.trim().length==0;
}

function stringMatch(searched, content){
    var keyWords = searched.split(" ");
    var strContent = content.toLowerCase();
    for(let i = 0; i < keyWords.length; i++){
        if(isEmpty(keyWords[i])){
            continue;
        }
        if(strContent.includes(keyWords[i])){
            return true;
        }
    }
    return false;
}

function searchForMateriel(value){
    if(isEmpty(value)){
        // Search bar empty --> Show everything
        var items = document.querySelectorAll(".materialItem");
        for(let i = 0; i < items.length; i++){
            items[i].style.display="";
        }
        var types = document.querySelectorAll(".materialType");
        for(let i = 0; i < types.length; i++){
            types[i].style.display="";
        }
    } else {
        // Filter results
        var types = document.querySelectorAll(".materialType");
        for(let typeIndex = 0; typeIndex < types.length; typeIndex++){
            var typeTitle = types[typeIndex].querySelector(".title").innerText;
            var items = types[typeIndex].querySelectorAll(".materialItem");
            if(stringMatch(value, typeTitle)){
                // Type match the query -> Show all in
                for(let i = 0; i < items.length; i++){
                    items[i].style.display="";
                }
                types[typeIndex].style.display = "";
            } else {
                var atLeastOneMatches = false;
                for(let i = 0; i < items.length; i++){
                    var itemDesc = items[i].querySelector(".materialItemTitle").innerText + " " +
                        items[i].querySelector(".materialItemDescription").innerText;

                    if(stringMatch(value, itemDesc)){
                        items[i].style.display="";
                        atLeastOneMatches = true;
                    } else {
                        items[i].style.display="none";
                    }
                }
                if(atLeastOneMatches){
                    types[typeIndex].style.display = "";
                } else {
                    types[typeIndex].style.display = "none";
                }
            }
        }
    }
}
