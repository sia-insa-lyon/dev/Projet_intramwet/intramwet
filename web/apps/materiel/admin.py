# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import TypeMateriel, ProprietaireMateriel, Materiel, Emprunt, EmpruntMateriel, EmpruntLocal, EmpruntElement


@admin.register(TypeMateriel)
class TypeMaterielAdmin(admin.ModelAdmin):
    list_display = ('typeMateriel',)


@admin.register(ProprietaireMateriel)
class ProprietaireMaterielAdmin(admin.ModelAdmin):
    list_display = ('nomProprietaire',)


@admin.register(Materiel)
class MaterielAdmin(admin.ModelAdmin):
    list_display = (
        'nom',
        'typeMateriel',
        'proprietaireMatos',
        'anneeAchat',
        'montantCaution',
        'prixLocation',
        'description',
        'quantite',
    )
    list_filter = ('typeMateriel', 'proprietaireMatos', 'anneeAchat')


@admin.register(Emprunt)
class EmpruntAdmin(admin.ModelAdmin):
    list_display = (
        'membre',
        'debut',
        'fin',
        'description',
    )
    list_filter = (
        'membre',
        'debut',
        'fin',
    )


@admin.register(EmpruntMateriel)
class EmpruntMaterielAdmin(admin.ModelAdmin):
    list_display = (
        'membre',
        'debut',
        'fin',
        'description',
    )
    list_filter = (
        'membre',
        'debut',
        'fin',
    )


@admin.register(EmpruntLocal)
class EmpruntLocalAdmin(admin.ModelAdmin):
    list_display = (
        'membre',
        'debut',
        'fin',
        'description',
        'statut',
    )
    list_filter = (
        'membre',
        'debut',
        'fin',
    )


@admin.register(EmpruntElement)
class EmpruntElementAdmin(admin.ModelAdmin):
    list_display = (
        'emprunt',
        'materiel',
        'statut',
        'quantite',
    )
    list_filter = ('emprunt', 'materiel', 'validePar')
