from django.urls import path

from .views import EmpruntLocalFeed, emprunt, emprunt_disponibilite, \
    emprunt_local, local_disponibilite, \
    local_gestion, local_statut, local_disponible, emprunt_gestion, \
    emprunt_statut, search_materiel, ajout_emprunt_element, gestion_materiel, \
    suppr_materiel, ajout_type_materiel_modal, modifier_materiel_modal, \
    modifier_materiel, ajout_type_materiel, get_photo_materiel, \
    materiel_description, supprimer_emprunt_materiel, supprimer_emprunt_local, \
    mes_emprunts_materiel, mes_emprunts_local

urlpatterns = [
    path('emprunt/', emprunt, name='emprunt'),
    path('emprunt/tache-<int:id_tache>', emprunt, name='emprunt'),
    path('emprunt/disponibilite/', emprunt_disponibilite,
         name='emprunt_disponibilite'),

    path('mes_emprunts/materiel/', mes_emprunts_materiel, name='mes_emprunts_materiel'),
    path('mes_emprunts/local/', mes_emprunts_local, name='mes_emprunts_local'),
    path('mes_emprunts/materiel/<int:pk>/supprimer', supprimer_emprunt_materiel,
         name='supprimer_emprunt_materiel'),
    path('mes_emprunts/local/<int:pk>/supprimer', supprimer_emprunt_local,
             name='supprimer_emprunt_local'),

    path('local/', emprunt_local, name='emprunt_local'),
    path('local/disponibilite/', local_disponibilite,
         name='local_disponibilite'),

    path('local/gestion/', local_gestion, name='local_gestion'),
    path('local/statut/', local_statut, name='local_statut'),
    path('api/local/calendrier/', EmpruntLocalFeed(), name='local_calendrier'),
    path('api/local/disponibilite', local_disponible),

    path('emprunt/gestion/', emprunt_gestion, name='emprunt_gestion'),
    path('emprunt/statut/', emprunt_statut, name='emprunt_statut'),
    path('materiel/search/', search_materiel, name='search_materiel'),
    path('emprunt/ajout/', ajout_emprunt_element,
         name='ajout_emprunt_element'),

    path('materiel/', gestion_materiel, name='materiel'),
    path('supprimer/', suppr_materiel, name="suppr_materiel"),
    path('ajout/type/modal', ajout_type_materiel_modal,
         name='ajout_type_materiel_modal'),
    path('modifier/modal', modifier_materiel_modal,
         name='modifier_materiel_modal'),
    path('modifier/', modifier_materiel, name='modifier_materiel'),
    path('ajout/type/', ajout_type_materiel, name='ajout_type_materiel'),
    path('photo/<int:materiel_id>', get_photo_materiel, name="photo_materiel"),

    path('description/', materiel_description,
         name='materiel_description'),
]
