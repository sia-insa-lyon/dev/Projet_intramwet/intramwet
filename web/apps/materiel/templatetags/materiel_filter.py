from django import template

register = template.Library()


@register.filter
def location(user):
    return not user.has_perm('utilisateur.emprunt_gratuit')
