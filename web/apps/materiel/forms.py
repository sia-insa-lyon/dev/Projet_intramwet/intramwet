from datetime import date, datetime, timezone

from django import forms
from django.forms import CharField
from django.forms import DateTimeField
from django.forms import Textarea
from django.utils.translation import gettext_lazy as _

from .models import Materiel
from .models import ProprietaireMateriel
from .models import TypeMateriel


class AjoutMateriel(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AjoutMateriel, self).__init__(*args, **kwargs)
        for field in iter(self.fields):
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })
        self.fields['nom'].widget.attrs.update(
            {'placeholder': 'Nom du matériel à ajouter'})
        self.fields['typeMateriel'].queryset = TypeMateriel.objects.all()
        self.fields[
            'proprietaireMatos'].queryset = ProprietaireMateriel.objects.all()
        self.fields['anneeAchat'].widget.attrs.update(
            {'placeholder': 'JJ/MM/AAAA',
             'class': 'form-control datetimepicker-input',
             'data-target': '#id_anneeAchat'})
        self.fields['montantCaution'].widget.attrs.update(
            {'value': '1000', 'step': '0.01'})
        self.fields['prixLocation'].widget.attrs.update(
            {'placeholder': '0.00€', 'step': '0.01'})
        self.fields['quantite'].widget.attrs.update({'value': '1'})

    class Meta:
        model = Materiel
        fields = ['nom', 'typeMateriel', 'proprietaireMatos', 'anneeAchat',
                  'montantCaution', 'prixLocation', 'photo', 'description',
                  'quantite']

        initial = {
            'anneeAchat': date.today(),
        }
        widgets = {
            'description': Textarea(
                attrs={'placeholder': 'Description du matériel', 'rows': 3}),
        }
        labels = {
            'typeMateriel': _('Type du matériel'),
            'proprietaireMatos': _('Propriétaire du matériel'),
            'anneeAchat': _('Date d\'achat'),
            'montantCaution': _('Montant de la caution')
        }


class DemandeDateEmprunt(forms.Form):
    debut = DateTimeField(label='Début de l\'emprunt')
    debut.widget.attrs = {'class': 'form-control datetimepicker-input',
                          'placeholder': 'JJ/MM/AAAA hh:mm',
                          'data-target': '#id_debut',
                          'data-toggle': 'datetimepicker',
                          'data-target-input': 'nearest'}
    fin = DateTimeField(label='Fin de l\'emprunt')
    fin.widget.attrs = {'class': 'form-control datetimepicker-input',
                        'placeholder': 'JJ/MM/AAAA hh:mm',
                        'data-target': '#id_fin',
                        'data-toggle': 'datetimepicker',
                        'data-target-input': 'nearest'}

    def is_valid(self):
        valid = super(DemandeDateEmprunt, self).is_valid()
        if not valid:
            return valid
        debut = self.cleaned_data['debut']
        fin = self.cleaned_data['fin']
        if debut > fin or debut == fin or debut < datetime.now(timezone.utc):
            self._errors['dates'] = 'Vos dates sont invalides !'
            return False

        return True


class DemandeEmprunt(DemandeDateEmprunt):
    raison = CharField(label='Merci de spécifier la raison de votre emprunt.',
                       widget=Textarea)
    raison.widget.attrs = {'class': 'form-control', 'rows': '3',
                           'placeholder': 'Donnez la raison de votre emprunt.'}
