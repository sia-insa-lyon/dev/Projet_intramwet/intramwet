# Generated by Django 3.0.5 on 2020-05-02 12:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Emprunt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('debut', models.DateTimeField()),
                ('fin', models.DateTimeField()),
                ('description', models.TextField(null=True)),
                ('commentaire', models.TextField(blank=True, null=True)),
                ('dateCommentaire', models.DateTimeField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ProprietaireMateriel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nomProprietaire', models.CharField(max_length=50, unique=True, verbose_name='nom du propriétaire')),
            ],
        ),
        migrations.CreateModel(
            name='TypeMateriel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('typeMateriel', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='EmpruntLocal',
            fields=[
                ('emprunt_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='materiel.Emprunt')),
                ('statut', models.CharField(choices=[('VAL', 'Validé'), ('REF', 'Refusé'), ('ATT', 'En attente')], default='ATT', max_length=3)),
                ('empruntGdM', models.BooleanField(default=False, verbose_name='Est un emprunt de GdM ?')),
            ],
            bases=('materiel.emprunt',),
        ),
        migrations.CreateModel(
            name='EmpruntMateriel',
            fields=[
                ('emprunt_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='materiel.Emprunt')),
            ],
            bases=('materiel.emprunt',),
        ),
        migrations.CreateModel(
            name='Materiel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(max_length=100)),
                ('anneeAchat', models.DateField(null=True)),
                ('montantCaution', models.FloatField(null=True)),
                ('prixLocation', models.FloatField(default=0, verbose_name='Prix de location du matériel')),
                ('photo', models.ImageField(null=True, upload_to='photos_matos/%Y/')),
                ('description', models.TextField(null=True)),
                ('quantite', models.IntegerField(default=1)),
                ('proprietaireMatos', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='materiel.ProprietaireMateriel')),
                ('typeMateriel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='materiel.TypeMateriel')),
            ],
        ),
        migrations.CreateModel(
            name='EmpruntElement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('statut', models.CharField(choices=[('VAL', 'Validé'), ('REF', 'Refusé'), ('ATT', 'En attente'), ('REN', 'Rendu')], default='ATT', max_length=20)),
                ('quantite', models.IntegerField(default=1)),
                ('commentaire', models.TextField(blank=True)),
                ('materiel', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='materiel.Materiel')),
            ],
        ),
    ]
