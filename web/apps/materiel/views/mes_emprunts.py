from datetime import datetime, timezone

from django.core.exceptions import PermissionDenied
from django.shortcuts import render, get_object_or_404, redirect

from ..decorators import can_access_materiel, can_access_local
from ..models import EmpruntMateriel, EmpruntLocal


@can_access_local
def mes_emprunts_local(request):
    emprunts_local_en_cours = EmpruntLocal.objects.filter(
        membre=request.user,
        fin__gte=datetime.now(timezone.utc)
    ).order_by('-fin').distinct()
    emprunts_local_finis = EmpruntLocal.objects.filter(
        membre=request.user,
        fin__lt=datetime.now(timezone.utc)
    ).order_by('-fin').distinct()
    return render(request, 'mes_emprunts_local.html', locals())


@can_access_materiel
def mes_emprunts_materiel(request):
    emprunts_materiel_en_cours = EmpruntMateriel.objects.filter(
        membre=request.user,
        fin__gte=datetime.now(timezone.utc)
    ).order_by('-fin').distinct()
    emprunts_materiel_finis = EmpruntMateriel.objects.filter(
        membre=request.user,
        fin__lt=datetime.now(timezone.utc)
    ).order_by('-fin').distinct()

    return render(request, 'mes_emprunts_materiel.html', locals())


@can_access_materiel
def supprimer_emprunt_materiel(request, pk: int):
    emprunt = get_object_or_404(EmpruntMateriel, pk=pk)
    if not (request.user.is_staff or request.user.is_superuser) or emprunt.membre != request.user:
        raise PermissionDenied
    emprunt.delete()
    return redirect('mes_emprunts')


@can_access_local
def supprimer_emprunt_local(request, pk: int):
    emprunt = get_object_or_404(EmpruntLocal, pk=pk)
    if not (request.user.is_staff or request.user.is_superuser) or emprunt.membre != request.user:
        raise PermissionDenied
    emprunt.delete()
    return redirect('mes_emprunts')
