from .api import EmpruntLocalFeed, local_disponible
from .gestion_emprunts import emprunt_gestion, emprunt_statut, ajout_emprunt_element, supprimer_emprunts
from .gestion_local import local_gestion, local_statut
from .gestion_materiel import gestion_materiel, search_materiel, suppr_materiel, \
    ajout_type_materiel_modal, modifier_materiel_modal, \
    ajout_type_materiel, modifier_materiel
from .local import emprunt_local, local_disponibilite
from .materiel import materiel_description, emprunt, \
    emprunt_disponibilite, get_photo_materiel
from .mes_emprunts import mes_emprunts_local, mes_emprunts_materiel, supprimer_emprunt_materiel, supprimer_emprunt_local
