from django.http import JsonResponse
from django.shortcuts import render

from ..decorators import can_access_local
from ..forms import DemandeEmprunt, DemandeDateEmprunt
from ..models import EmpruntLocal
from apps.conf.bot import Bot, Email
from ..utils import notify_new_emprunt_local


@can_access_local
def emprunt_local(request):
    alerts = []
    if request.method == 'POST':
        form = DemandeEmprunt(request.POST)
        if form.is_valid():
            debut = form.cleaned_data.get('debut')
            fin = form.cleaned_data.get('fin')
            raison = form.cleaned_data.get('raison')

            if EmpruntLocal.local_est_emprunte(debut, fin):
                alerts.append({
                    'type': 'danger',
                    'titre': 'Emprunt : ',
                    'contenu': 'Le local n\'est pas disponible sur vos dates !'
                })
            else:
                emprunt = EmpruntLocal(debut=debut, fin=fin,
                                       description=raison, membre=request.user)
                emprunt.save()

                # Emprunt réussi !
                alerts.append({
                    'type': 'success',
                    'titre': 'Emprunt : ',
                    'contenu': 'Votre demande d\'emprunt a été prise en compte. Un administrateur vous contactera prochainement.'
                })
                form = DemandeEmprunt()
                Bot('emprunt_local', channel=Bot.CHAN_BUREAU,
                    data=locals()).send()
                Email('emails/demande_emprunt.html', request.user.email,
                      locals()).send()
                notify_new_emprunt_local(request, emprunt)

        else:
            alerts.append({
                'type': 'danger',
                'titre': 'Emprunt : ',
                'contenu': 'Votre demande est invalide. Merci de remplir correctement chaque champ.'
            })

    else:
        form = DemandeEmprunt()

    return render(request, "emprunt_local.html", locals())


@can_access_local
def local_disponibilite(request):
    demande = DemandeDateEmprunt(request.POST)
    if demande.is_valid():
        debut = demande.cleaned_data.get("debut")
        fin = demande.cleaned_data.get("fin")

        return JsonResponse({'disponible': not EmpruntLocal.local_est_emprunte(debut, fin)})
    else:
        return JsonResponse({})
