from datetime import datetime

from django.http import JsonResponse
from django.urls import reverse
from django_ical.views import ICalFeed

from ..models import EmpruntLocal


class EmpruntLocalFeed(ICalFeed):
    product_id = '-//medialamouette.fr//Local//FR'
    timezone = 'UTC+2'
    file_name = 'local.ics'

    def items(self):
        return EmpruntLocal.objects.all().order_by('debut')

    def item_guid(self, item: EmpruntLocal):
        return f'emprunt_local_{item.id}'

    def item_title(self, item: EmpruntLocal):
        return f'Emprunt du local par {item.membre.nomComplet}'

    def item_description(self, item: EmpruntLocal):
        return item.description

    def item_start_datetime(self, item: EmpruntLocal):
        return item.debut

    def item_end_date_time(self, item: EmpruntLocal):
        return item.fin

    def item_link(self, item: EmpruntLocal):
        return reverse("emprunt_local")


def local_disponible(request) -> JsonResponse:
    date_format = "%d/%m/%Y %H:%M"
    date_debut = datetime.strptime(request.GET.get("date_debut"), date_format)
    date_fin = datetime.strptime(request.GET.get("date_fin"), date_format)
    return JsonResponse({
        'disponible': not EmpruntLocal.local_est_emprunte(
            date_debut=date_debut,
            date_fin=date_fin),
        'pas-en-attente': not EmpruntLocal.local_est_emprunte(
            date_debut=date_debut,
            date_fin=date_fin,
            strict=False)
    })
