from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string

from apps.conf.utils import media_response
from ..decorators import can_access_materiel
from ..forms import DemandeEmprunt, DemandeDateEmprunt
from ..models import Materiel, EmpruntElement, EmpruntMateriel
from apps.conf.bot import Email, Bot
from apps.projet.models import Tache
from ..utils import notify_new_emprunt


@can_access_materiel
def emprunt(request, id_tache=-1):
    """
        Renvoie la page d'une demande d'emprunt.
        Traite un formulaire de demande d'emprunt.
    """

    alerts = []
    if request.method == 'POST':
        demande_valide = True
        form = DemandeEmprunt(request.POST)
        if form.is_valid():
            debut = form.cleaned_data.get('debut')
            fin = form.cleaned_data.get('fin')
            raison = form.cleaned_data.get('raison')

            matos_demande = []
            for e in request.POST.getlist('materiel'):
                id = int(e)
                quant = int(request.POST.get('quantite-' + str(id)))
                matos_demande.append((id, quant))

            if not matos_demande:
                demande_valide = False
                alerts.append({
                    'type': 'danger',
                    'titre': 'Emprunt : ',
                    'contenu': 'Vous devez sélectionner un élément pour pouvoir le réserver !'
                })

            for m in matos_demande:
                quant_rest = Materiel.objects.get(id=m[0]).quantite_restante(
                    date_debut=debut, date_fin=fin)
                if quant_rest < m[1]:
                    demande_valide = False
                    alerts.append({
                        'type': 'danger',
                        'titre': 'Emprunt : ',
                        'contenu': 'Un élément demandé ({}) n\'est pas disponible sur vos dates !'.format(
                            Materiel.objects.get(id=m[0]).nom)
                    })
        else:
            demande_valide = False
            alerts.append({
                'type': 'danger',
                'titre': 'Emprunt : ',
                'contenu': 'Votre demande est invalide. Merci de remplir correctement chaque champ.'
            })

        if demande_valide:
            emprunt = EmpruntMateriel(membre=request.user, debut=debut,
                                      fin=fin, description=raison)
            emprunt.save()

            for m in matos_demande:
                emprunt_elem = EmpruntElement(emprunt=emprunt,
                                              materiel=Materiel.objects.get(
                                                  id=m[0]), quantite=m[1],
                                              statut=EmpruntElement.EN_ATTENTE)
                emprunt_elem.save()

            # Emprunt réussi !
            alerts.append({
                'type': 'success',
                'titre': 'Emprunt : ',
                'contenu': 'Votre demande d\'emprunt a été prise en compte. Un administrateur vous contactera prochainement.'
            })

            # Notifications
            Email('emails/demande_emprunt.html', request.user.email,
                  locals()).send()
            Bot('emprunt', channel=Bot.CHAN_BUREAU, data=locals()).send()
            notify_new_emprunt(request, emprunt)

            form = DemandeEmprunt()

            # Liaison au système de tâches
            try:
                tache = Tache.objects.get(id=id_tache)
                tache.emprunts.add(emprunt)
            except Tache.DoesNotExist:
                pass

            return JsonResponse({
                "valid": True,
                "alerts": render_to_string('emprunt_alertes.html',
                                           {'alerts': alerts})
            })

        return JsonResponse({
            "valid": False,
            "alerts": render_to_string('emprunt_alertes.html',
                                       {'alerts': alerts})
        })

    else:
        form = DemandeEmprunt()

    materiels = Materiel.objects.empruntable_categorise(request.user)

    tache = None
    if id_tache != -1:
        try:
            tache = Tache.objects.get(id=id_tache)
        except Tache.DoesNotExist:
            tache = None

    return render(request, 'emprunt.html',
                  {'materiels': materiels, 'form': form, 'alerts': alerts,
                   'tache': tache})


@can_access_materiel
def emprunt_disponibilite(request):
    """
        Réponse à une requête AJAX
        Permet de connaître le matos disponible, ainsi que sa quantité restante,
        en fonction de ce à quoi l'utilisateur peut accéder.
        Renvoie les données au format JSON sous la forme :
        {
            idMateriel1 : {'dispo' : True, 'quantite' : 2}
            idMateriel2 : {'dispo' : False}
            ...
        }
    """

    demande = DemandeDateEmprunt(request.POST)
    if demande.is_valid():
        debut = demande.cleaned_data.get("debut")
        fin = demande.cleaned_data.get("fin")

        matos_disponible = Materiel.objects.disponible(
            utilisateur=request.user, date_debut=debut, date_fin=fin)
    else:
        matos_disponible = {}
    return JsonResponse(matos_disponible)


@can_access_materiel
def materiel_description(request):
    if request.method == 'GET':
        if 'id' in request.GET:
            desc = Materiel.objects.get(id=request.GET.get('id')).description
            return JsonResponse({'description': desc})

    return JsonResponse({'description': 'Pas de description !'})


@can_access_materiel
def get_photo_materiel(request, materiel_id: int):
    """
    Permet aux utilisateurs connectés de récupérer les photos de matériel

    :param request:
    :param materiel_id: id du matériel dont on veut récupérer la photo
    :return:
    """
    materiel = get_object_or_404(Materiel, id=materiel_id)
    return media_response(materiel.photo_url)
