from apps.conf.utils import get_prefix
from apps.materiel.models import EmpruntMateriel, Emprunt, EmpruntLocal


def _notify(request, emprunt: Emprunt, template: str):
    from ..conf.bot import Email
    from ..utilisateur.models import MembreCA

    prefix = get_prefix(request)
    for email in MembreCA.objects.emails_to_notify_for_emprunt():
        Email(template, email, locals()).send()

def notify_new_emprunt(request, emprunt: EmpruntMateriel):
    _notify(request, emprunt, 'emails/notification_emprunt_bureau.html')

def notify_new_emprunt_local(request, emprunt: EmpruntLocal):
    _notify(request, emprunt, 'emails/notification_emprunt_local_bureau.html')