from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied


def can_access_materiel(function):
    def wrap(request, *args, **kwargs):
        logged_in_func = login_required(function)
        if request.user.has_perm('utilisateur.acceder_materiel'):
            return logged_in_func(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def can_manage_materiel(function):
    def wrap(request, *args, **kwargs):
        return staff_member_required(can_access_materiel(function))(request, *args, **kwargs)
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def can_access_local(function):
    def wrap(request, *args, **kwargs):
        logged_in_func = login_required(function)
        if request.user.has_perm('utilisateur.acceder_local'):
            return logged_in_func(request, *args, **kwargs)
        else:
            raise PermissionDenied
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def can_manage_local(function):
    def wrap(request, *args, **kwargs):
        return staff_member_required(can_access_local(function))(request, *args, **kwargs)
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
