# Intramwet Helm Chart

## Values

### Values

A modifier

| Value | Default | Comment |
|:------|---------|---------|
| `image.repository` | registry.gitlab.com/sia-insa-lyon/openraideur:latest ||
| `tags.postgresql`| true | Wether to deploy a postgresql Helm Chart or not |
| `image.pullPolicy` | Always ||
| `autoscaling.enabled` | false ||
| `autoscaling.minReplicas` | 1 ||
| `autoscaling.maxReplicas` | 5 ||
| `autoscaling.targetCPUUtilizationPercentage` | 90 ||
| `resources.request.cpu` | 300m ||
| `resources.request.memory` | 500m ||
| `resources.limits.cpu` | 500m ||
| `resources.limits.memory` | 1Gi ||
| `ingress.enabled` | true ||
| `ingress.enabled` | portail.asso-insa-lyon.fr ||
| `service.type` | ClusterIP ||
| `service.port` | 80 ||
| `secret.mgApiKey` | nil
| `env.serverEmail` | noreply@bde-insa-lyon.fr ||
| `env.adminEmail` | bde.equipe.orgaif.dev@led.insa-lyon.fr ||
| `env.apiDns` | portail.asso-insa-lyon.fr ||
| `env.mailgunDomain` | mg.asso-insa-lyon.fr ||
| `env.appDebug` | `False` ||
| `env.djangoSettingsModule` | portailva.settings ||
| `env.defaultFromEmail` | no-reply@asso-insa-lyon.fr ||
| `env.siteDNS` | portail.asso-insa-lyon.fr ||
| `env.siteUrl` | https://portail.asso-insa-lyon.fr ||
| `env.wsgiApp` | portailva.wsgi ||
| `env.bdePhone` | 0472438229 ||
| `storage.sizeMedia` | 5Gi ||

### Database Values

| Value | Default | Comment |
|:------|---------|---------|
| `postgresql.enabled` | true | set to `false`if you don't wand to deploy a postgresql database |
| `postgresqlPassword` | postgres | use this value if you don't deploy a postgresql db to give django the db password |
| `postgresql.postgresqlDatabase` | django ||
| `postgresql.postgresqlHost` | postgresql-0 ||
| `postgresql.persitence.accessModes` | [ReadWriteMany] ||
| `postgresql.persitence.size` | 1Gi ||

### Backup Values

A faire
