#!/bin/bash

docker-compose down
docker system prune -f
docker volume prune -f
docker-compose up -d --build
docker-compose run web python /web/manage.py migrate
docker-compose run web python /web/manage.py collectstatic
docker-compose down
