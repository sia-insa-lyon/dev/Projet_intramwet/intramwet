#!/bin/bash

#
# Restores the database using the specified database parameters
# Usage: sh restore.sh docker-compose.yml postgres/.env /backups/<year>/<month>/<filename>
#

# Reads the parameters from the specified env file
. $2
export DB_NAME DB_USER DB_SERVICE DB_PASS DB_PORT

# Restores the database
docker-compose -f $1 run postgres_backup pg_restore -h $DB_SERVICE -U $DB_USER -d $DB_NAME -O -v $3
docker-compose down
